#!/usr/bin/python3
"""Basic parser for psalms as downloaded and converted to tex.
Format is:
{XX~} half verse * half verse
continue half verse

Last two verses are Gloria.
"""

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("PATH", help="File to read in.")
parser.add_argument("RANGE", help="Range to print")
parser.add_argument("-g", "--gloria", help="Print Gloria?", action="store_true")
args = parser.parse_args()

verses = {}

with open(args.PATH,"r") as f:
    for line in f:
        if line.startswith("{"):
            no, vpart = line.split("}", 1)
            no = int(no.lstrip("{").strip(".~"))
            verses[no] = vpart.strip("\n").replace("~†","~\+\\\\\n \vin").replace("~*","~\*\\\\\n \vin")
            last_verse = no
        else:
            verses[last_verse] = verses[last_verse] + " " + line.strip("\n")

start,stop = args.RANGE.split("-")
            
for i in range(int(start),int(stop)+1):
    print(verses[i])

if args.gloria:
    for i in list(verses.keys())[-2:]:
        print(verses[i])
            
        
        

#for verse in find_all(r"(\{[0-9]+~\})(.*?)",)


