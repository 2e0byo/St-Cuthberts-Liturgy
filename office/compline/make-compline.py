#!/usr/bin/python3
# a script to assemble all the required tex files
import os
import shutil
import csv
import subprocess
import jinja2
import readchar  # install with pip if not already installed

makefile_env = jinja2.Environment(
    autoescape=False, loader=jinja2.FileSystemLoader(os.path.abspath('.')))

template_makefile = makefile_env.get_template('template-makefile')

latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%%%%',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(os.path.abspath('.')))
template = latex_jinja_env.get_template('template-compline.tex')


def be_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    os.chdir(directory)


def prompt(message, y, n):  # case-insensitive
    y = y.lower()
    n = n.lower()
    while 1:
        print(message + ' ')
        choice = readchar.readchar().lower()
        #        choice = raw_input(message+' ').lower()
        if choice == y or choice == n:
            break
        else:
            print('Illegal input, try again')
    if choice == y:
        return True
    else:
        return False


seasons = ['Ordinary-Time', 'Lent', 'Eastertide', 'Advent', 'Christmastide']
days = [
    'Saturdays or Vigils of Solemnities', 'Sundays or Solemnities', 'Mondays',
    'Tuesdays', 'Wednesdays', 'Thursdays', 'Fridays'
]
variants = {}


def make_command(path):
    c = '\\'
    for i in path.split('_'):
        if '-' in i:
            a = i.split('-')
            c += a[0].title() + a[1]
        else:
            c += i.title()
    return (c)


# setup absolute symlinks (we'll make relative later if possible)
# NB we do this on the fly to keep it portable
# can't copy relative symlinks, you see...
template_dir = 'template-absolute'

shutil.copytree('template', template_dir, symlinks=True)
os.chdir(template_dir)
for f in os.listdir('./'):
    if os.path.islink(f):  # convert to absolute
        target = os.path.realpath(os.readlink(f))
        os.remove(f)
        os.symlink(target, f)
os.chdir('../')

for season in seasons:
    print('Processing', season, '...')
    # read season data
    with open(season + '.csv', 'r') as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            #  print(row)
            variants[row[0]] = row[1:9]
    be_dir(season)
    for lit_day in days:
        day = lit_day.split()[0].strip('s')
        print('  ..Processing', day, '...')
        compline_file = 'Compline' + season + day + '.tex'
        compline_file_pdf = 'Compline' + season + day + '.pdf'
        psalms = variants[lit_day][2]
        antiphon1 = variants[lit_day][3]
        antiphon2 = variants[lit_day][4]
        if antiphon2 == '':
            antiphon2 = antiphon1
        responsory = variants[lit_day][5]
        nunc = variants[lit_day][6]
        nunc_command = make_command(nunc)
        antiphon = variants[lit_day][7]  # ?
        if os.path.exists(day):
            shutil.rmtree(day)
        shutil.copytree('../' + template_dir, day, symlinks=True)
        os.chdir(day)
        # convert back to relative symlinks
        for f in os.listdir('./'):
            if os.path.islink(f):
                target = os.path.relpath(os.readlink(f))
                os.remove(f)
                os.symlink(target, f)

        with open(compline_file, 'w') as f:
            if int(psalms) == 2:
                if antiphon1 != antiphon2:
                    ant1 = '\\ant[1] ' + antiphon1 + '\n'
                    psalms1 = '\\Psalms' + day + 'One'
                    ant2 = '\\ant ' + antiphon1 + '\n'
                    ant3 = '\\ant[2] ' + antiphon2 + '\n'
                    psalms2 = '\\Psalms' + day + 'Two'
                    ant4 = '\\ant ' + antiphon2 + '\n'
                else:
                    ant1 = '\\ant ' + antiphon1 + '\n'
                    psalms1 = '\\Psalms' + day + 'One'
                    ant2 = ''
                    ant3 = ''
                    psalms2 = '\\Psalms' + day + 'Two'
                    ant4 = '\\ant ' + antiphon2 + '\n'
            else:
                ant1 = '\\ant ' + antiphon1 + '\n'
                psalms1 = '\\Psalms' + day + 'One'
                ant2 = '\\ant ' + antiphon1 + '\n'
                ant3 = ''
                psalms2 = ''
                ant4 = ''

            if season == 'Advent':
                antiphon_versicle = '\\AlmaAdventVersicle'
            elif season == 'Christmastide':
                antiphon_versicle = '\\AlmaChristmastideVersicle'
            else:
                antiphon_versicle = '\\' + antiphon.title() + 'Versicle'
            # Write Tex File using the Template
            f.write(
                template.render(
                    lit_day=lit_day,
                    day=day,
                    season=season.replace('-',' '),
                    intro=variants[lit_day][0],
                    hymn_chant=variants[lit_day][1],
                    hymn_command=make_command(variants[lit_day][1]),
                    ant1=ant1,
                    psalms1=psalms1,
                    ant2=ant2,
                    ant3=ant3,
                    psalms2=psalms2,
                    ant4=ant4,
                    responsory=responsory,
                    nunc=nunc,
                    nunc_command=nunc_command,
                    antiphon=antiphon,
                    antiphon_translation='\\' + antiphon.title() +
                    'Translation',
                    antiphon_versicle=antiphon_versicle))

        # write makefile
        with open('makefile', 'w') as f:
            f.write(
                template_makefile.render(
                    compline_file=compline_file,
                    compline_file_pdf=compline_file_pdf,
                    compline_file_book=compline_file_pdf.strip('.pdf') +
                    '-book.pdf'))
        os.chdir('../')  # next day

    os.chdir('../')  # Next Season

if prompt('Force-recompile all files? y/n', 'y', 'n'):
    compile_errors = {}
    print('\n   *** WARNING!! ***')
    print(
        '   This will be somewhat slow, as it will compile everything *twice* (to allow for vertical heights).  I suggest you get a cup of tea...\n'
    )
    for season in seasons:
        compile_errors[season] = []
        print('Compiling', season, '...')
        be_dir(season)
        for lit_day in days:
            day = lit_day.split()[0].strip('s')
            print('  ..Processing', day, '...')
            compline_file = 'Compline' + season + day + '.tex'
            compline_file_pdf = 'Compline' + season + day + '.pdf'
            be_dir(day)
            try:
                # run *silently*--logs to file
                subprocess.run(
                    [
                        'lualatex', '--shell-escape',
                        '--interaction=batchmode', compline_file
                    ],
                    check=True,
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)
            except subprocess.CalledProcessError:
                compile_errors[season].append(day)
            try:
                # run *silently*--logs to file
                subprocess.run(
                    [
                        'lualatex', '--shell-escape',
                        '--interaction=batchmode', compline_file
                    ],
                    check=True,
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)
                subprocess.run(
                    ['pdfbook', compline_file_pdf],
                    check=True,
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)  # create booklet
            except subprocess.CalledProcessError:
                if day not in compile_errors[season]:
                    compile_errors[season].append(day)
        #  try: shutil.copy(compline_file_pdf,'../../'+season+day+'.pdf')
        # except FileNotFoundError:
        #     print("!!no pdf found!!")
            os.chdir('../')
        os.chdir('../')

    for season in seasons:
        if len(compile_errors[season]) > 0:
            print('Errors were encountered in ' + season)
            for day in compile_errors[season]:
                print('  ..' + day +
                      ' ==> Check compline.log file in directory')
# cleanup code
shutil.rmtree(template_dir)
