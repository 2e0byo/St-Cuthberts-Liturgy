#!/usr/bin/python3

# library to generate svg files from gabc with a template
import subprocess
from time import time
import os
from shutil import copy, rmtree
import jinja2


class ComplileException(Exception):
    pass


latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%%%%',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(
        os.path.dirname(os.path.abspath(__file__)))  # load from script dir
)


def tex2pdf(texfile):
    # run *silently*--logs to file
    # throws subprocess.CalledProcessError if anything goes wrong
    subprocess.run(
        ['lualatex', '--shell-escape', '--interaction=batchmode', texfile],
        check=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL)


def make_tmp_dir():
    tmpdir = 'gabv2svg-tmp-' + str(int(time()))
    os.mkdir(tmpdir)
    return (tmpdir)


def pdf2svg(pdffile):
    # throws subprocess.CalledProcessError if anything goes wrong
    subprocess.run(
        ['pdf2svg', pdffile, pdffile.rstrip('pdf') + 'svg'],
        check=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL)


def croppdf(pdffile):
    subprocess.run(
        ['pdfcrop', pdffile, pdffile],
        check=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL)


def gabc2svg(gabcfile, template):
    global CompileException
    """
    main function to convert gabc files to svgs.  Uses tmpdir in
    calling dir.  gabcfile should be passable to os.path, template is
    jinja2 template object.  It will be passed the argument
    gabcfile=gabcfile only.
    """
    tmpdir = make_tmp_dir()
    wd = os.getcwd()

    base = os.path.basename(gabcfile).rstrip(".gabc")
    outdir = os.path.dirname(os.path.abspath(gabcfile))
    os.chdir(tmpdir)
    with open(base + '.tex', 'w') as f:
        f.write(template.render(gabcfile=gabcfile))
    try:
        tex2pdf(base + ".tex")
        croppdf(base + ".pdf")
        pdf2svg(base + ".pdf")
        copy(base + ".svg", outdir)
        copy(base + ".pdf", outdir)  # some people can't handle svgs
    except subprocess.CalledProcessError:
        raise CompileException('error! unable to convert ' + gabcfile + " see "
                               + os.path.abspath(tmpdir))
    os.chdir(wd)  # go back
    print('removing temporary directory: ', tmpdir)
    rmtree(tmpdir)


def generate_template(template_file):
    """
    generate template object from relative path to template_file
    """
    global latex_jinja_env
    template = latex_jinja_env.get_template(template_file)
    return (template)
