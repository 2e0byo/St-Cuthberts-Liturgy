#!/usr/bin/python3
import gabc2svg
import os
template = gabc2svg.generate_template('template-gabc2svg.tex')
for root, dirs, files in os.walk(".", topdown=False):
    for file_ in [f for f in files if f.endswith('.gabc')]:
        gabc_file = os.path.join(root, file_)
        print(gabc_file)
        svgfile = gabc_file.rstrip(".gabc") + ".svg"
        pdffile = gabc_file.rstrip(".gabc") + ".pdf"
        if (os.path.exists(svgfile)) and (os.path.exists(
                pdffile)):  # don't recompile if newer than source
            if (os.path.getmtime(svgfile) > os.path.getmtime(gabc_file)) and (
                    os.path.getmtime(pdffile) > os.path.getmtime(gabc_file)):
                print('skipping ' + gabc_file)
                continue
        gabc2svg.gabc2svg(gabc_file, template)
