#+Title: Office according to the 1962 rubrics and vulgate psalter

If you just want to pray the office, head over to
[[http://www.divinumofficium.com][diviniumofficium.com]]. Currently this folder only contains Compline.

Needless to say, files in this folder are in Latin.  I may get round
to adding translations at some point. 
