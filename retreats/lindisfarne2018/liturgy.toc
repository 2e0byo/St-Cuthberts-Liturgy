\changetocdepth {2}
\contentsline {section}{Friday}{1}{section*.1}
\contentsline {subsection}{Orthodox Little Compline}{1}{section*.2}
\contentsline {subsubsection}{The Lesser Doxology}{7}{section*.3}
\contentsline {subsubsection}{The Creed}{8}{section*.4}
\contentsline {subsubsection}{Prayer to the Most Holy Theotokos\FN@sf@gobble@opt {`The one who gave birth to God': that is, Mary. The usual Western rendering is `Mother of God'. In the course of the Christological controversy, the title was affirmed by the Third Ecumenical Council of Ephesus in 431: the stress was on the status of the one born and only indirectly on His mother.}}{14}{section*.5}
\contentsline {subsubsection}{A Prayer to Our Lord Jesus Christ}{15}{section*.6}
\contentsline {subsubsection}{Ending Prayer to the Theotokos}{16}{section*.7}
\contentsline {subsection}{About Taizé}{17}{section*.8}
\contentsline {subsection}{Taizé service}{19}{section*.9}
\contentsline {subsubsection}{Bless the Lord}{19}{section*.10}
\contentsline {subsubsection}{I Am Sure I Shall See}{19}{section*.11}
\contentsline {subsubsection}{Alleluia}{21}{section*.13}
\contentsline {subsubsection}{Gospel}{22}{section*.14}
\contentsline {subsubsection}{\hfill \normalfont \emph {Luke 10:38--42}}{22}{section*.15}
\contentsline {subsubsection}{Staňte Se Solí Země}{23}{section*.16}
\contentsline {subsubsection}{Silence}{23}{section*.17}
\contentsline {subsubsection}{Our Father}{24}{section*.18}
\contentsline {subsubsection}{Behüte mich, Gott}{25}{section*.19}
\contentsline {subsubsection}{Laudate Dominum}{26}{section*.20}
\contentsline {subsubsection}{Da Pacem (Canon)}{27}{section*.21}
\contentsline {subsection}{Anglican Compline}{28}{section*.22}
\contentsline {subsubsection}{Preparation}{28}{section*.23}
\contentsline {subsubsection}{Hymn}{28}{section*.24}
\contentsline {subsubsection}{The Word of God}{29}{section*.25}
\contentsline {subsubsection}{Scripture Reading\hfill \normalfont \emph {Jeremiah 14:9}}{30}{section*.26}
\contentsline {subsubsection}{Responsory}{30}{section*.27}
\contentsline {subsubsection}{Gospel Canticle}{30}{section*.28}
\contentsline {subsubsection}{Prayers}{30}{section*.29}
\contentsline {subsubsection}{The Conclusion}{31}{section*.30}
\contentsline {section}{Saturday}{32}{section*.31}
\contentsline {subsection}{Catholic Compline}{32}{section*.32}
\contentsline {subsubsection}{Introduction to the Office}{32}{section*.33}
\contentsline {subsubsection}{Penitential Act}{32}{section*.34}
\contentsline {subsubsection}{Hymn}{33}{section*.35}
\contentsline {subsubsection}{Psalmody}{33}{section*.36}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {Deuteronomy 6:4-7}}{34}{section*.37}
\contentsline {subsubsection}{Responsory}{35}{section*.38}
\contentsline {subsubsection}{Nunc Dimittis}{35}{section*.39}
\contentsline {subsubsection}{Prayer}{36}{section*.40}
\contentsline {subsubsection}{Blessing}{36}{section*.41}
\contentsline {subsubsection}{Marian Antiphon}{37}{section*.42}
