%% Generated by lilypond-book
%% Options: [indent=0\mm,line-width=348\pt]
\include "lilypond-book-preamble.ly"


% ****************************************************************
% Start cut-&-pastable-section
% ****************************************************************



\paper {
  indent = 0\mm
  line-width = 348\pt
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
}

\layout {
  
}





% ****************************************************************
% ly snippet:
% ****************************************************************
\sourcefilename "./beheute/beheute.ly"
\sourcefileline 0

%% LilyPond file generated by Denemo version 2.0.8

%%http://www.gnu.org/software/denemo/

\version "2.18.0"

AutoBarline = {}
AutoEndMovementBarline = \bar "|."

				% The music follows

MvmntIVoiceI = {\voiceOne
		\partial 256*32  g'8\AutoBarline
		\bar ".|:" g'4 f'8 g' ees'2\AutoBarline
		ees'8 ees' aes' g' f'4. f'8\AutoBarline
		bes'4 bes'8 g' aes'4. bes'8\AutoBarline
				%5
		c''4 bes'4. c''8 d'' bes'\AutoBarline
		g'4 bes' c'' bes'8 aes'\AutoBarline
		g'4 f'2 r8 g' \bar ":|."}


MvmntIVoiceII = {\voiceTwo
		 \partial 256*32  ees'8\AutoBarline
		 \bar ".|:" ees'4 d'8 d' c'2\AutoBarline
		 c'8 c' c' ees' d'4. d'8\AutoBarline
		 d'4 d'8 d' f'4. ees'8\AutoBarline
				%5
		 ees'4 ees'4. ees'8 f' f'\AutoBarline
		 ees'4 ees' ees' ees'8 ees'\AutoBarline
		 ees'4 d'2 r8 ees' \bar ":|."}


MvmntIVoiceIII = {\voiceThree
		  \partial 256*32  bes8\AutoBarline
		  \bar ".|:" bes4 bes8 bes g2\AutoBarline
		  g8 g f f f4. f8\AutoBarline
		  g4 bes8 bes c'4. bes8\AutoBarline
				%5
		  aes4 g4. g8 bes bes\AutoBarline
		  c'4 g aes aes8 aes\AutoBarline
		  bes4 bes2 r8 bes \bar ":|."}


MvmntIVoiceIV = {\voiceFour
		 \partial 256*32  ees8\AutoBarline
		 \bar ".|:" ees4 bes,8 bes, c2\AutoBarline
		 c8 c f f bes,4. bes,8\AutoBarline
		 g,4 g8 g f4. g8\AutoBarline
				%5
		 aes4 ees4. ees8 d d\AutoBarline
		 c4 bes, aes, c8 c\AutoBarline
		 bes,4 bes,2 r8 ees \bar ":|."}



OrigLyrics = \lyricmode {
  Be -- hü -- te mich, Gott, ich ver -- trau -- e dir, du zeigst mir den Weg zum Le -- ben.  Bei dir ist Freu -- de, Frue -- de in Fül -- le. Be-
}
EnglishLyrics = \lyricmode {

  (Keep me, O__ _ God, for I tryst in you.  You show me the path of
  life. _  with you there is full -- ness, full -- ness of joy. Keep)

}



#(set-global-staff-size 20)
\paper {
  myStaffSize = #18
    #(define fonts
    (make-pango-font-tree "TeX Gyre Pagella"
                          "TeX Gyre Pagella"
     "TeX Gyre Pagella"
     (/ myStaffSize 20)
                           ))
   indent = 0\cm} 

\score { %Start of Movement
  <<

				%Start of Staff
    \new Staff   << 
      \new Voice = "VoiceIMvmntI"  { 
	\clef treble    \key ees \major    \time 4/4   \MvmntIVoiceI
      } %End of voice
      \new Voice = "VoiceIIMvmntI"  { 
	\MvmntIVoiceII
      } %End of voice

    >> %End of Staff
    \new Lyrics = OrigLyrics \lyricsto VoiceIMvmntI \OrigLyrics
    \new Lyrics = EnglishLyrics \lyricsto VoiceIMvmntI \EnglishLyrics

				%Start of Staff
    \new Staff  << 
      \new Voice = "VoiceIIIMvmntI"  { 
	\clef bass    \key ees \major    \time 4/4   \MvmntIVoiceIII
      } %End of voice
      \new Voice = "VoiceIVMvmntI"  { 
	\MvmntIVoiceIV
      } %End of voice

    >> %End of Staff

  >>
  \midi { }
  \layout { }

} %End of Movement






% ****************************************************************
% end ly snippet
% ****************************************************************
