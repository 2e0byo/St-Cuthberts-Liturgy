\changetocdepth {2}
\contentsline {section}{Friday}{1}
\contentsline {subsection}{Compline}{1}
\contentsline {subsubsection}{Introduction to the Office}{1}
\contentsline {subsubsection}{Penitential Act}{1}
\contentsline {subsubsection}{Hymn}{2}
\contentsline {subsubsection}{Psalmody}{2}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {Jeremiah 14:9}}{3}
\contentsline {subsubsection}{Responsory}{4}
\contentsline {subsubsection}{Nunc Dimittis}{4}
\contentsline {subsubsection}{Prayer}{5}
\contentsline {subsubsection}{Blessing}{5}
\contentsline {subsubsection}{Marian Antiphon}{5}
\contentsline {section}{Saturday}{7}
\contentsline {subsection}{Lauds}{7}
\contentsline {subsubsection}{Introduction to the Office}{7}
\contentsline {subsubsection}{Hymn}{7}
\contentsline {subsubsection}{Psalmody}{8}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {Hebrews 13:7-8}}{9}
\contentsline {subsubsection}{Responsory}{9}
\contentsline {subsubsection}{Benedictus}{10}
\contentsline {subsubsection}{Intercessions}{10}
\contentsline {subsubsection}{Concluding Prayer}{11}
\contentsline {subsection}{Taizé}{11}
\contentsline {subsubsection}{Gospel\hfill \normalfont \emph {1 John 2:7-10}}{11}
\contentsline {subsection}{Compline}{12}
\contentsline {subsubsection}{Introduction to the Office}{12}
\contentsline {subsubsection}{Penitential Act}{12}
\contentsline {subsubsection}{Hymn}{13}
\contentsline {subsubsection}{Psalmody}{13}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {Deuteronomy 6:4-7}}{14}
\contentsline {subsubsection}{Responsory}{14}
\contentsline {subsubsection}{Nunc Dimittis}{15}
\contentsline {subsubsection}{Prayer}{15}
\contentsline {subsubsection}{Blessing}{16}
\contentsline {subsubsection}{Marian Antiphon}{16}
\contentsline {section}{Sunday}{18}
\contentsline {subsection}{Lauds}{18}
\contentsline {subsubsection}{Introduction to the Office}{18}
\contentsline {subsubsection}{Hymn}{18}
\contentsline {subsubsection}{Psalmody}{19}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {2 Tim 2:8,11-13}}{21}
\contentsline {subsubsection}{Responsory}{22}
\contentsline {subsubsection}{Benedictus}{22}
\contentsline {subsubsection}{Intercessions}{22}
\contentsline {subsubsection}{Concluding Prayer}{23}
\contentsline {subsection}{Lectio Divina}{23}
\contentsline {subsubsection}{Gospel\hfill \normalfont \emph {Matthew 25:1-13}}{23}
