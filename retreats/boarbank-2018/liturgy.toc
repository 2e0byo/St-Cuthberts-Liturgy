\changetocdepth {2}
\contentsline {section}{Friday}{1}
\contentsline {subsection}{Compline}{1}
\contentsline {subsubsection}{Introduction to the Office}{1}
\contentsline {subsubsection}{Penitential Act}{1}
\contentsline {subsubsection}{Hymn}{2}
\contentsline {subsubsection}{Psalmody}{2}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {Jeremiah 14:9}}{4}
\contentsline {subsubsection}{Responsory}{4}
\contentsline {subsubsection}{Nunc Dimittis}{4}
\contentsline {subsubsection}{Prayer}{5}
\contentsline {subsubsection}{Blessing}{5}
\contentsline {subsubsection}{Marian Antiphon}{6}
\contentsline {section}{Saturday}{8}
\contentsline {subsection}{Lauds}{8}
\contentsline {subsubsection}{Introduction to the Office}{8}
\contentsline {subsubsection}{Hymn}{8}
\contentsline {subsubsection}{Psalmody}{9}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {2 Corinthians 1:3-5}}{11}
\contentsline {subsubsection}{Responsory}{11}
\contentsline {subsubsection}{Benedictus}{11}
\contentsline {subsubsection}{Intercessions}{12}
\contentsline {subsubsection}{Concluding Prayer}{12}
\contentsline {subsection}{Mass}{12}
\contentsline {subsubsection}{First Reading\hfill \normalfont \emph {Apocalypse 11:4--12}}{12}
\contentsline {subsubsection}{Responsorial Psalm\hfill \normalfont \emph {Psalm 143(144):1--2,9--10}}{13}
\contentsline {subsubsection}{Gospel Acclamation\hfill \normalfont \emph {}}{13}
\contentsline {subsubsection}{Gospel\hfill \normalfont \emph {Luke 20:27--40}}{14}
\contentsline {subsection}{Compline}{14}
\contentsline {subsubsection}{Introduction to the Office}{14}
\contentsline {subsubsection}{Penitential Act}{15}
\contentsline {subsubsection}{Hymn}{15}
\contentsline {subsubsection}{Psalmody}{16}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {Deuteronomy 6:4-7}}{17}
\contentsline {subsubsection}{Responsory}{17}
\contentsline {subsubsection}{Nunc Dimittis}{17}
\contentsline {subsubsection}{Prayer}{18}
\contentsline {subsubsection}{Blessing}{18}
\contentsline {subsubsection}{Marian Antiphon}{19}
\contentsline {section}{Sunday}{21}
\contentsline {subsection}{Lauds}{21}
\contentsline {subsubsection}{Introduction to the Office}{21}
\contentsline {subsubsection}{Hymn}{21}
\contentsline {subsubsection}{Psalmody}{23}
\contentsline {subsubsection}{Reading\hfill \normalfont \emph {Ephesians 4:15-16}}{25}
\contentsline {subsubsection}{Responsory}{25}
\contentsline {subsubsection}{Benedictus}{25}
\contentsline {subsubsection}{Intercessions}{26}
\contentsline {subsubsection}{Concluding Prayer}{27}
\contentsline {subsection}{Mass}{27}
\contentsline {subsubsection}{First Reading\hfill \normalfont \emph {Daniel 1:13--14}}{27}
\contentsline {subsubsection}{Responsorial Psalm\hfill \normalfont \emph {Psalm 92(93):1--2,5}}{27}
\contentsline {subsubsection}{Second Reading\hfill \normalfont \emph {Apocalypse 1:5--8}}{28}
\contentsline {subsubsection}{Gospel Acclamation\hfill \normalfont \emph {}}{28}
\contentsline {subsubsection}{Gospel\hfill \normalfont \emph {John 18:33--7}}{28}
