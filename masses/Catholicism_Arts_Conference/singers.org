#+Title: Music for Singers

For convenience here are links to the
[[./liturgy.pdf][Missalette]] containing the chant, the
[[./newsham_mass/lilypond/proofreading_edition.pdf][setting]] and the
[[./webbe-o-jesu/processed_webbe-o-jesu.pdf][anthem]].

To download .pdfs, click on the link and then the icon of a cloud with
an arrow pointing downwards in the top right-hand corner of the
resulting window.
