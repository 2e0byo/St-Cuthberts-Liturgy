
\subsection{Introduction}


\noindent Welcome to St. Cuthbert's Chapel for this \emph{Missa
  Cantata}.  The Mass, as celebrated here today, stands in continuity
with the earliest Christian liturgies and with those of today, but
after the nineteenth century (as in the fourth) there are liturgical
changes small and great which can be quite remarkable when one first
encounters them.  Musically: we are using a nineteenth century setting
by Mgr. Charles Newsham, rector of this college between 1837 and
1863. In characteristically rich homophony, with the organ bulking out
the texture at every conceivable point, from its sudden \emph{fortes}
and \emph{pianos} to its lugubrious \emph{tempi} it represents and era
which prized expression over subtelty and whose tastes were distinctly
grandiose.  But the chant sung alongside it bears witness to the other
side of nineteenth century Catholicism.  This is pre
`gregorian-revival' chant, pre-Solesmes, with its lilting cadences and
ever-expanding notational subtelties.  It is printed very simply;
there are only really three neumes.  It is sung extremely slowly, and
perhaps (as we will sing it today) with no ornamentation or
improvisation; and it is sung with a full voice, without the monastic
subtelty commonly heard in Gregorian singing today.  Together these
elements create an impression of timelessness~---~common also to the
extremely slow, if highly ornate, chants of the Eastern Orthodox~---~and
of transcendence, standing alongside Mgr. Newsham's rather emotional,
human devotion.

These two elements~---~emotional devotion (the anthem text is, again
characteristically, simply a string of crescendoing titles for Jesus)
and transcendent simplicity~---~meet in the roles assigned to
congregation, priest and servers.  For those who have not encountered
the ancient liturgies, eastern and western, the first surprising
discovery is simultaneity: frequently the priest is doing or saying
one thing whilst the choir, perhaps with the congregation, is singing
something else.  Here a sharp distinction between clergy and laity is
certainly introduced, but not~---~so it was felt~---~a competition.  The
Mass `by priest and people sungen' is very much offered by both; it is
not merely a private Mass with spectators.  As the innumerable
`guides' and `methods' for laypeople explained, the people offer `by
contemplation' what the priest offers `by words and actions'~---~so the
most popular counter-reformation handbook, Gother's \emph{Instructions
  and devotions for hearing mass}, printed anonymously in 1696 and
reprinted periodically after.  Ushaw has several examples, some
rebound, with the names, marginalia and family trees of their recusant
owners inscribed in successive hands, a witness to their popularity.

But this idea of `contemplation' is not a nineteenth century
innovation~---~(Pseudo-)Dionysius' word for `laypeople' in the
\emph{Ecclesiastical} and \emph{Celestial Hierarchies} is
`contemplative' and recent scholarship (notably Golitzin) has
re-emphasised the liturgical nature of this contemplation.  What is
striking in nineteenth century practice even for those accustomed to
the \emph{Usus Antiquior} today is the \emph{complete} silence of the
congregation.  This was not universal~---~even up to the eve of Vatican
II the level of verbal congregational participation varied wildly
between churches, as it does in the East to this day.  Yet at Ushaw in
the mid-nineteenth century they were silent.  The people are here to
`hear' the Mass; the priest to `say' (or sing) it.  But the people
were encouraged not merely to pray their own private devotions, but to
follow along the texts, to pray them and `join themselves' spiritually~---~%
psychologically and emotionally~---~to the offering being made.

To enable those present today to have a sense of what this
contemplative participation might have meant, this booklet has been
prepared.  It contains the entire text of the Mass, in Latin and a
slavishly literal English translation, whose purpose is to illuminate
the depth of meaning in the Latin text. (Those accustomed to English
liturgies may be surprised to discover that by Christ's merely
\emph{partaking} of our nature, we are raised to something
higher~---~becoming \emph{consortes}~---~with His.)  It is typeset as
nearly as possible in the same style as texts of the period would have
been, and reproduces the chants sung by the Choir to showcase their
visual simplicity.  Changes in congregational posture are indicated in
capitals in the rubrics, although it would have been relatively common
to kneel for the whole Mass, and those less able to stand are welcome
to kneel or sit throughout.


Ushaw have asked that photographs not be taken.  Lastly, in the spirit
of contemplation, you are asked to switch your phones off or to
`silent', to preserve this `world apart' where the only sounds and
gestures are sacred.

\bigskip

\hfill John Morris

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "liturgy"
%%% End:
