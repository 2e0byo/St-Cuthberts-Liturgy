#+Title: Newsham’s Mass Setting

This folder contains Newsham’s Mass setting, retypset in Lilypond with
the aid of Denemo.  For now, pdfs are in ~./lilypond~

* [75%] Progress
** DONE Kyrie [100%]
   :LOGBOOK:
   CLOCK: [2019-03-01 Fri 18:08]--[2019-03-01 Fri 18:13] =>  0:05
   CLOCK: [2019-02-22 Fri 13:16]--[2019-02-22 Fri 13:16] =>  0:00
   :END:
- [X] Notes
- [X] Lilypond-ing
- [X] Lyrics
- [X] Dynamics
- [X] Tempi
- [X] Proofread
** DONE Gloria [100%]
   :LOGBOOK:
   CLOCK: [2019-03-04 Mon 14:15]--[2019-03-04 Mon 14:15] =>  0:00
   CLOCK: [2019-03-04 Mon 14:01]--[2019-03-04 Mon 13:40] => -1:39
   CLOCK: [2019-03-04 Mon 12:41]--[2019-03-04 Mon 12:50] =>  0:09
   CLOCK: [2019-02-22 Fri 11:41]--[2019-02-22 Fri 13:11] =>  1:30
   CLOCK: [2019-02-22 Fri 10:28]--[2019-02-22 Fri 11:01] =>  0:33
   CLOCK: [2019-02-21 Thu 13:55]--[2019-02-21 Thu 13:56] =>  0:01
   :END:
- [X] Notes
- [X] Lilypond-ing
- [X] Lyrics
- [X] Dynamics
- [X] Tempi
- [X] Proofread
** DONE Credo [100%]
   :LOGBOOK:
   CLOCK: [2019-03-04 Mon 12:51]--[2019-03-04 Mon 13:06] =>  0:15
   CLOCK: [2019-02-28 Thu 16:14]--[2019-02-28 Thu 17:29] =>  1:15
   CLOCK: [2019-02-21 Thu 14:34]--[2019-02-21 Thu 15:37] =>  1:03
   CLOCK: [2019-02-21 Thu 14:28]--[2019-02-21 Thu 14:30] =>  0:02
   CLOCK: [2019-02-21 Thu 14:11]--[2019-02-21 Thu 14:25] =>  0:14
   :END:
- [X] Notes
- [X] Lilypond-ing
- [X] Lyrics
- [X] Dynamics
- [X] Tempi
- [X] Proofread

** DONE Sanctus [100%]
   :LOGBOOK:
   CLOCK: [2019-03-04 Mon 13:22]--[2019-03-04 Mon 13:28] =>  0:06
   CLOCK: [2019-02-28 Thu 17:29]--[2019-02-28 Thu 18:17] =>  0:48
   CLOCK: [2019-02-21 Thu 15:37]--[2019-02-21 Thu 15:57] =>  0:20
   :END:
- [X] Notes
- [X] Lilypond-ing
- [X] Lyrics
- [X] Dynamics
- [X] Tempi
- [X] Proofread

** DONE Agnus [100%]
    :LOGBOOK:
    CLOCK: [2019-03-04 Mon 13:36]--[2019-03-04 Mon 13:44] =>  0:08
    CLOCK: [2019-03-01 Fri 17:45]--[2019-03-01 Fri 18:03] =>  0:18
    CLOCK: [2019-03-01 Fri 17:12]--[2019-03-01 Fri 17:41] =>  0:29
    CLOCK: [2019-02-21 Thu 17:14]--[2019-02-21 Thu 17:35] =>  0:21
    CLOCK: [2019-02-21 Thu 16:03]--[2019-02-21 Thu 16:14] =>  0:11
    CLOCK: [2019-02-21 Thu 15:58]--[2019-02-21 Thu 16:00] =>  0:02
    CLOCK: [2019-02-21 Thu 15:57]--[2019-02-21 Thu 15:57] =>  0:00
    :END:
 - [X] Notes
 - [X] Lilypond-ing
 - [X] Lyrics
 - [X] Dynamics
 - [X] Tempi
 - [X] Proofread

** DONE Proofreading Score
   :LOGBOOK:
   CLOCK: [2019-03-01 Fri 19:23]--[2019-03-01 Fri 19:48] =>  0:25
   CLOCK: [2019-03-01 Fri 18:20]--[2019-03-01 Fri 19:16] =>  0:56
   :END:

** TODO Proofreading edits
   :LOGBOOK:
   CLOCK: [2019-03-05 Tue 15:18]--[2019-03-05 Tue 15:18] =>  0:00
   CLOCK: [2019-03-04 Mon 22:54]--[2019-03-04 Mon 23:05] =>  0:11
   CLOCK: [2019-03-04 Mon 14:58]--[2019-03-04 Mon 15:06] =>  0:08
   :END:
** TODO Full edition

* Lisp Code
So far we’re stripping buffers down with the following growing bit of
code.

Then just delete all the formatting and put ~\include template.ly~ at the end.

#+BEGIN_SRC elisp
(defun process-lilypond-file (beg end)
  (interactive "*r")
  (save-restriction
    (narrow-to-region beg end)
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward "\\([a-z]+?'*?,*?[0-9]*?*\\.*?~*?(\\)" nil t)
	(replace-match "\\\\autoBeamOn \\1"))
      (goto-char (point-min))
      (while (re-search-forward "\\([a-z]+?'*?,*?[0-9]*?\\.*?~*?)\\)" nil t)
	(replace-match "\\1 \\\\autoBeamOff"))
      (goto-char (point-min))
      (while (re-search-forward "MvmntIVoiceI " nil t)
	(replace-match "TrebleMusic"))
      (goto-char (point-min))
      (while (re-search-forward "MvmntIVoiceII " nil t)
	(replace-match "AltoMusic"))
      (goto-char (point-min))
      (while (re-search-forward "MvmntIVoiceIII " nil t)
	(replace-match "TenorMusic"))
      (goto-char (point-min))
      (while (re-search-forward "MvmntIVoiceIV " nil t)
	(replace-match "BassMusic"))
      (goto-char (point-min))
      (while (re-search-forward "%.*?%}" nil t) ;denemo's links
	(replace-match ""))
      (goto-char (point-min))
      (while (re-search-forward "\\\\AutoBarline" nil t) ;denemo's links
	(replace-match ""))
      (goto-char (point-min))
      (while (re-search-forward "\\\\AutoEndMovementBarline" nil t) ;denemo's links
	(replace-match "\\\\bar \"|.\""))
      (goto-char (point-min))
      )))
#+END_SRC
