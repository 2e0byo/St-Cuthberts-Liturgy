origLineBreak={}
origPageBreak={}
origNoLineBreak={}
% lineBreakSetting =
% #(define-music-function
%   (parser location l)
%   (ly:music?)			%is it?
%   #{
%   \autoLineBreaksOff
%   #}
% )
\paper{
  top-margin = 5\cm
  bottom-margin = 3\cm
  ragged-last-bottom = ##t
}
\header{
  title = "Mgr Newsham's Mass"
  subtitle = "Typeset John Morris"
}

\include "kyrie.ly"
\include "gloria.ly"
\include "credo.ly"
\include "sanctus.ly"
\include "agnus.ly"