\version "2.18.2"

\score {
  \new ChoirStaff <<
    \new Staff = "Treble" \with {instrumentName = "Treble"} <<
      \new Voice = "Treble"  { 
	\initialKeyTime
	\clef "treble"
	\TrebleMusic
      }
    >>
    \new Lyrics \lyricsto "Treble" \TrebleLyrics

    \new Staff = "Alto" \with {instrumentName = "Alto"} <<
      \new Voice = "Alto"  { 
	\initialKeyTime
	\clef "treble"	
	\AltoMusic
      }
    >>
    \new Lyrics \lyricsto "Alto" \AltoLyrics

    \new Staff = "Tenor" \with {instrumentName = "Tenor"} <<
      \new Voice = "Tenor"  { 
	\initialKeyTime
	\clef "treble_8"		
	\TenorMusic
      }
    >>
    \new Lyrics \lyricsto "Tenor" \TenorLyrics

    \new Staff = "Bass" \with {instrumentName = "Bass"} <<
      \new Voice = "Bass"  { 
	\initialKeyTime
	\clef "bass"	
	\BassMusic
      }
    >>
    \new Lyrics \lyricsto "Bass" \BassLyrics


  >>
  \header {
    peice = "TEST"
  }

  \layout {
    \context {
      \Score
      \override DynamicText.direction = #UP
      \override DynamicLineSpanner.direction = #UP
    }
    \context {
      \Staff
      \RemoveEmptyStaves
    }
  }
}
