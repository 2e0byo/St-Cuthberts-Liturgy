\include "dynamics.ly"
				% The music follows

TrebleMusic = {
  \tempo "Largo" 4 = 48
  bes'4.\f bes'8 c''2
  bes'4. bes'8 a'2
  bes'4. bes'8 c''4 d''
  ees''4. ees''8 d''2
				%5
  bes'4. a'8 g'2 \origLineBreak\origPageBreak
  c''4. bes'8 a'4 bes'
  g'1
  f'4 d''\p \autoBeamOn c''( bes') \autoBeamOff
  a' a' \autoBeamOn bes'( c'') \autoBeamOff
				%10
  d'' bes' \autoBeamOn a'( g') \autoBeamOff
  fis'1
  bes'4.\f bes'8 c''2
  d''4. d''8 ees''2
  \autoBeamOn d''4( a' bes' d'') \autoBeamOff
				%15
  c''4.( d''8) \autoBeamOff c''4  d''4
  \autoBeamOn c''( bes') \autoBeamOff a' a'
  \autoBeamOn bes'(  c''4) \autoBeamOff d''2
  ees''4. ees''8 d''4 bes'
  c''1
				%20
  bes' \bar "||"
  bes'2\p bes'4 c''
  bes'4.( g'8) \autoBeamOff bes'2
  g' g'4 aes'\origLineBreak
  \autoBeamOn f'8( g') \autoBeamOff aes'4 g'2
				%25
  \autoBeamOn ees''4( d'') \autoBeamOff c'' bes'
  \autoBeamOn c''( bes') \autoBeamOff aes'2
  g'2.( c''4) \autoBeamOff
  bes' a' bes'2 \bar "||"\origLineBreak
  bes'4.\f bes'8 c''2
				%30
  bes'4. bes'8 a'2
  bes'4. bes'8 c''4 d''
  ees''4. ees''8 d''2
  bes'4. a'8 g'2\origLineBreak
  c''4. bes'8 a'4 bes'
				%35
  g'1
  f'4 d''4\p \autoBeamOn c''( bes') \autoBeamOff
  a' a' \autoBeamOn bes'( c'') \autoBeamOff
  d'' bes' \autoBeamOn a'( g') \autoBeamOff\origLineBreak
  fis'1
				%40
  bes'4.\f bes'8 c''2
  d''4. d''8 ees''2
  \autoBeamOn d''4( a' bes' d'') \autoBeamOff
  c''4.( d''8) \autoBeamOff c''4 d''
  \autoBeamOn c''( bes') \autoBeamOff a' a'
				%45
  \autoBeamOn bes'( c'') \autoBeamOff d''2
  ees''4. ees''8 d''4 bes'
  c''1
  bes' \bar "||"}

AltoMusic = {
  f'4.\f f'8 f'2
  f'4. e'8 f'2
  f'4. f'8 f'4 f'
  f'4. f'8 f'2
				%5
  f'4. f'8 \autoBeamOn ees'4( f') \autoBeamOff
  ees'4. e'8 f'4 f'
  \autoBeamOn f'( e'8 d' e'2) \autoBeamOff
  f'4 f'\p \autoBeamOn c'( d'8 e') \autoBeamOff
  f'4 \autoBeamOn f'2( ees'4) \autoBeamOff
				%10
  \autoBeamOn d'2( ees') \autoBeamOff
  d'1
  d'4.\f d'8 f'2
  f'4. f'8 f'2
  f'1
				%15
  f'2 f'4  f'4
  \autoBeamOn c'( d'8 e') \autoBeamOff f'4 f'
  \autoBeamOn f'( ees') \autoBeamOff d'2
  f'4. f'8 f'4 f'
  \autoBeamOn g'2( f'4) \autoBeamOff ees'
				%20
  d'1
  r
  r
  ees'2\p ees'4 ees'
  \autoBeamOn d'8( ees') \autoBeamOff f'4 ees'2
				%25
  ees' ees'4 ees'
  ees'2. d'4
  ees'2.( g'4) \autoBeamOff
  f'4. ees'8 d'2
  f'4.\f f'8 f'2
				%30
  f'4 e' f'2
  f'4. f'8 f'4 f'
  f'4. f'8 f'2
  f'4. f'8 \autoBeamOn ees'4( f') \autoBeamOff
  ees'4. e'8 f'4 f'
				%35
  \autoBeamOn f'( e'8 d' e'2) \autoBeamOff
  f'4 f'4\p \autoBeamOn c'( d'8 e') \autoBeamOff
  f'4 \autoBeamOn f'2( ees'4) \autoBeamOff
  \autoBeamOn d'2( ees') \autoBeamOff
  d'1
				%40
  d'4.\f d'8 f'2
  f'4. f'8 f'2
  f'1
  f'2 f'4 f'
  \autoBeamOn c'( d'8 e') \autoBeamOff f'4 f'
				%45
  \autoBeamOn f'( ees') \autoBeamOff d'2
  f'4. f'8 f'4 f'
  \autoBeamOn g'2( f'4 ees') \autoBeamOff
  d'1 \bar "|."
}


TenorMusic = {
  d'4.\f d'8 c'2
  d'4. c'8 c'2
  bes4. bes8 a4 bes
  c'4. c'8 bes2
				%5
  bes4. bes8 \autoBeamOn bes4( b) \autoBeamOff
  c'4. c'8 c'4 d'
  \autoBeamOn c'2( g8 a bes4) \autoBeamOff
  c' r r2
  r4 c'^\boxP bes4.( a8
				%10
  bes4 d' c' bes) \autoBeamOff
  a1
  bes4.\f bes8 \autoBeamOn bes4( a) \autoBeamOff
  bes4. bes8 c'2
  \autoBeamOn bes4( c' bes2) \autoBeamOff
				%15
  a4.( bes8) \autoBeamOff a4 r
  r2 r4  c'4
  bes4.( a8) \autoBeamOff bes2
  c'4. c'8 bes4 bes
  \autoBeamOn bes( a8 g) \autoBeamOff a2
				%20
  bes1
  g2\p g4 aes
  g2 g
  bes c'4 c'
  bes2 bes
				%25
  \autoBeamOn c'4( bes) \autoBeamOff aes g
  \autoBeamOn aes( bes) \autoBeamOff \autoBeamOn c'( bes) \autoBeamOff
  \autoBeamOn bes2( c') \autoBeamOff
  d'4 c' bes2
  d'4.\f d'8 c'2
				%30
  d'4 c' c'2
  bes4. bes8 a4 bes
  c'4. c'8 bes2
  bes4. bes8 \autoBeamOn bes4( b) \autoBeamOff
  c'4. c'8 c'4 d'
				%35
  \autoBeamOn c'2( g8 a bes4) \autoBeamOff
  a r r2
  r4 c'4^\boxP bes4.( a8
  bes4 d' c' bes) \autoBeamOff
  a1
				%40
  bes4.\f bes8 \autoBeamOn bes4( a) \autoBeamOff
  bes4. bes8 c'2
  \autoBeamOn bes4( c' bes2) \autoBeamOff
  a4.( c'8) \autoBeamOff bes4 r
  r2 r4 c'
				%45
  bes4.( a8) \autoBeamOff bes2
  c'4. c'8 bes4 bes
  \autoBeamOn bes( a8 g a2) \autoBeamOff
  bes1 \bar "|."
}


BassMusic = {
  bes4.\f bes8 a2
  g4. g8 \autoBeamOn f4( ees) \autoBeamOff
  d4. d8 d4 bes,
  a,4. a,8 bes,2
				%5
  d4. d8 \autoBeamOn ees4( d) \autoBeamOff
  c4. c8 f4 bes,
  c1
  f4 \autoBeamOn bes(\p a g
  f ees d a,) \autoBeamOff
				%10
  \autoBeamOn bes,2( c) \autoBeamOff
  d1
  g4.\f g8 f2
  bes,4. bes,8 \autoBeamOn bes,4( a,) \autoBeamOff
  \autoBeamOn bes,( ees d bes,) \autoBeamOff
				%15
  f2 f4  bes4
  \autoBeamOn a( g) \autoBeamOff f ees
  \autoBeamOn d( c) \autoBeamOff bes,2
  a,4. a,8 bes,4 d
  ees2 f
				%20
  bes,1
  ees2\p ees4 ees
  ees2 ees
  ees c4 aes,
  bes,2 ees
				%25
  \autoBeamOn c4( g) \autoBeamOff aes bes
  \autoBeamOn aes( g) \autoBeamOff f bes,
  \autoBeamOn ees( d c ees
  f2) \autoBeamOff bes,
  bes4.\f bes8 a2
				%30
  g4. g8 \autoBeamOn f4( ees) \autoBeamOff
  d4. d8 c4 bes,
  a,4. a,8 bes,2
  d4. d8 \autoBeamOn ees4( d) \autoBeamOff
  c4. c8 f4 bes,
				%35
  c1
  f4 \autoBeamOn bes4(\p a g
  f ees d c) \autoBeamOff
  \autoBeamOn bes,2( c) \autoBeamOff
  d1
				%40
  g4.\f g8 f2
  bes,4. bes,8 \autoBeamOn bes,4( a,) \autoBeamOff
  \autoBeamOn bes,( ees d bes,) \autoBeamOff
  f2 f4 bes
  \autoBeamOn a( g) \autoBeamOff f ees
				%45
  \autoBeamOn d( c) \autoBeamOff bes,2
  a,4. a,8 bes,4 d
  \autoBeamOn ees2( f) \autoBeamOff
  bes,1 \bar "|."
}


TrebleLyrics = \lyricmode { 
  Ky -- ri -- e Ky -- ri -- e Ky -- ri -- e e -- le -- i -- son
  Ky -- ri -- e ky -- ri -- e e -- lei -- son e -- lei -- son
  e -- lei -- son e -- lei -- son.
  Ky -- ri -- e Ky -- ri -- e e -- lei -- son
  e -- lei -- son e -- lei -- son
  Ky -- ri -- e e -- lei -- son.

  Chris -- te e -- lei -- son
  Chris -- te e -- le -- i -- son
  Chris -- te e -- lei -- son e -- le -- i -- son.

  Ky -- ri -- e Ky -- ri -- e Ky -- ri -- e e -- le -- i -- son
  Ky -- ri -- e ky -- ri -- e e -- lei -- son e -- lei -- son
  e -- lei -- son e -- lei -- son.
  Ky -- ri -- e Ky -- ri -- e e -- lei -- son
  e -- lei -- son e -- lei -- son
  Ky -- ri -- e e -- lei -- son.
}

AltoLyrics = \lyricmode { 
  Ky -- ri -- e Ky -- ri -- e Ky -- ri -- e e -- le -- i -- son
  Ky -- ri -- e __ ky -- ri -- e e -- lei -- son e -- lei -- son
  e -- lei -- son.
  Ky -- ri -- e Ky -- ri -- e e -- lei -- son
  e -- lei -- son e -- lei -- son
  Ky -- ri -- e e -- le -- i -- son.

  Chris -- te e -- lei -- son
  Chris -- te e -- le -- i -- son
  e -- le -- i -- son.

  Ky -- ri -- e Ky -- ri -- e Ky -- ri -- e e -- le -- i -- son
  Ky -- ri -- e __ ky -- ri -- e e -- lei -- son e -- lei -- son
  e -- lei -- son.
  Ky -- ri -- e Ky -- ri -- e e -- lei -- son
  e -- lei -- son e -- lei -- son
  Ky -- ri -- e e -- lei -- son.
}

TenorLyrics = \lyricmode { 
  Ky -- ri -- e Ky -- ri -- e Ky -- ri -- e e -- le -- i -- son
  Ky -- ri -- e __ ky -- ri -- e e -- lei -- son e -- lei -- son
  Ky -- ri -- e __ Ky -- ri -- e e -- lei -- son
  e -- lei -- son 
  Ky -- ri -- e e -- le -- i -- son.

  Chris -- te e -- lei -- son
  Chris -- te e -- lei -- son
  Chris -- te e -- lei -- son e -- le -- i -- son.

  Ky -- ri -- e Ky -- ri -- e Ky -- ri -- e e -- le -- i -- son
  Ky -- ri -- e __ ky -- ri -- e e -- lei -- son e -- lei -- son.
  Ky -- ri -- e __ Ky -- ri -- e e -- lei -- son
  e -- lei -- son
  Ky -- ri -- e e -- lei -- son.
}

BassLyrics = \lyricmode { 
  Ky -- ri -- e Ky -- ri -- e Ky -- ri -- e e -- le -- i -- son
  Ky -- ri -- e __ ky -- ri -- e e -- lei -- son e -- lei -- son
  Ky -- ri -- e Ky -- ri -- e e -- lei -- son
  e -- lei -- son e -- lei -- son
  Ky -- ri -- e e -- le -- i -- son.

  Chris -- te e -- lei -- son
  Chris -- te e -- lei  -- son
  Chris -- te e -- lei -- son e -- le  -- son.

  Ky -- ri -- e Ky -- ri -- e __ Ky -- ri -- e e -- le -- i -- son
  Ky -- ri -- e __ ky -- ri -- e e -- lei -- son e -- lei -- son.
  Ky -- ri -- e Ky -- ri -- e e -- lei -- son
  e -- lei -- son e -- lei -- son
  Ky -- ri -- e e -- lei -- son.
}

initialKeyTime = {\autoBeamOff \key bes \major \time 4/4}
pieceTitle = "Kyrie"

\include "template.ly"