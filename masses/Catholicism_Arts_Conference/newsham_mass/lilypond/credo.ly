\include "dynamics.ly"
				% The music follows

TrebleMusic = {
  \tempo "Moderato" 4 = 100
  bes'4\f bes'8 bes' c''4 bes'
  bes'2 a'
  d''4 d''8 d'' ees''4 d''
  d''2 c''4 c''\origLineBreak
				%5
  d''4 bes' a' bes'8 bes'
  g'2 f'4 f'8 f'
   f'4. f'8 \autoBeamOn f'4( bes') \autoBeamOff 
  bes'4. bes'8 a'4 c''~ 
   c'' d'' ees'' \autoBeamOn d''8( c'') \autoBeamOff \origLineBreak
				%10
   \autoBeamOn d''4( c''8.)  \autoBeamOff c''16\noBeam bes'2
  d''4. bes'8 g'4 bes'
   \autoBeamOn bes' a'8( g') \autoBeamOff  a'2
   c'' \autoBeamOn e'4( f') \autoBeamOff 
  a'2 g'\origLineBreak
				%15
  a'4 a'8 a' bes'4 d''
  g'4. bes'8 bes'8. bes'16 a'4
  c''4. c''8 c''4 c''
  d''2 c''
  a'4 a' a' a'8 a'\origLineBreak
				%20
  bes'4. bes'8 a'2
   c'' \autoBeamOn c''8( bes') \autoBeamOff \autoBeamOn a'( g') \autoBeamOff 
   \autoBeamOn f'4.( g'16 a') \autoBeamOff  g'2
   \autoBeamOn a'4( c'') \autoBeamOff \autoBeamOn c''8( bes') \autoBeamOff \autoBeamOn a'( g') \autoBeamOff 
   f'4. \autoBeamOn g'16( a') \autoBeamOff  g'2\origLineBreak
				%25
  c''4 \autoBeamOn a'2( c''4) \autoBeamOff 
   d''2 d''4 \autoBeamOn c''8( bes') \autoBeamOff 
  a'4 a' g'2
  f'1
  bes'4. bes'8 bes'4 a'\origLineBreak
				%30
  bes' d'' c'' d''
  ees'' d'' c'' bes'
  bes' a' d'' bes'
  a'8. a'16 bes'4 g'4. g'8
  a'2 r4 a'\origLineBreak
				%35
  bes'2 c''4 bes'
  bes'4. bes'8 a'4 c''
  d'' d'' ees'' d''8 d''
  d''2 c''4 c''\f
  d''2 d''4 d''\origLineBreak
				%40
  bes'2 bes'4 bes'
  ees'' ees''2 ees''4
   \autoBeamOn c''( d'') \autoBeamOff  bes' c''
   \autoBeamOn bes'2( a') \autoBeamOff 
  bes'1 \bar "|."\origLineBreak\origPageBreak
				%45
  \tempo "Adagio" 4 = 40
  \key f \major c''2\p \autoBeamOn cis''8( d'') \autoBeamOff \autoBeamOn bes'( g') \autoBeamOff 
   f'4. \autoBeamOn g'16( f') \autoBeamOff  e'4 g'
   a'2  bes'8([ a')]  bes'([ d'')]
   \autoBeamOn g'4.( a'16 bes') \autoBeamOff  a'2
  c''4. f'8 e'4 f'
				%50
  bes'4. bes'8 a'2\origLineBreak
  d''4. g'8 g'4 bes'
   bes' \autoBeamOn a'8( g') \autoBeamOff  a'4 c''
   \autoBeamOn cis''8( d'') \autoBeamOff  d''2 bes'4
   \autoBeamOn b'8( c'') \autoBeamOff  c''2 f'4
				%55
  \autoBeamOn g'2~(  g'8 bes' ) \autoBeamOff a'([  g')]
  a'4. g'8 f'2 \bar "|."\origLineBreak
    \tempo "Largo" 4 = 48
  f'2\mf f'
  g' a'
  g'4. g'8 f'4 f'
				%60
  bes'2 a'4 a'
  g'4. g'8 a'4 a'
  g'1\origLineBreak\origPageBreak
  f'1
  c''4.(\p bes'8) a'2
				%65
  a'4.( g'8) f'2
  f'\pp f'
  f' e'
  f'1 \bar "||" \origLineBreak
  \tempo "Allegro Moderato" 4 = 108
  \time 3/4  \key bes \major f'4\f g' f'
				%70
  bes'2 d''4
  ees''4. ees''8 d''4
  d'' c'' d''
  bes' bes' c''
  bes' a' f'8 f'\origLineBreak
				%75
  f'4 bes' d''
   d''4.( ees''8) \autoBeamOff  d''4
  d'' c'' bes'
  a'4. a'8 bes'4
   \autoBeamOn c''2( bes'4) \autoBeamOff 
				%80
  a'2 c''4\origLineBreak
  c''8. c''16 c''4 d''
  bes'8. bes'16 bes'4 c''
  a' a'8 a' a' a'
  bes'2 bes'4
				%85
  g' g' bes'
  bes'8. a'16 a'4 c''8 c''\origLineBreak
  c''4 a' c''
   d''2 \autoBeamOn c''8( bes') \autoBeamOff 
  a'2.
				%90
  g'2 c''8\p c''
  f'4 g' a'
  bes'2 a'4
  a'2 a'4\origLineBreak
  g'2 c''4
				%95
  f' g' a'
  bes' a' c''8 c''
  c''4 a' c''8 c''
   d''4 d'' \autoBeamOn c''8( bes') \autoBeamOff 
   \autoBeamOn a'2( g'4) \autoBeamOff\origLineBreak
				%100
  f'2 f'8\f f'
  f'4 bes' a'
  bes'8. bes'16 d''4 bes'8 bes'
   \autoBeamOn a'4( bes') \autoBeamOff  g'
  g' f' f'8 f'
				%105
  bes'4 a' bes'\origLineBreak
  g' f' f'8 f'
   \autoBeamOn f'4( bes') \autoBeamOff  d''
  c'' d'' ees''
   \autoBeamOn bes'2( a'4) \autoBeamOff 
				%110
  bes'2.
  bes'4\p bes' g'
  c'' c'' c''\origLineBreak
  bes'2 aes'4
  g'2.
				%115
  bes'4 bes' g'
  c''8. c''16 c''4 c''
   \autoBeamOn f'( g') \autoBeamOff  aes'
  g'2 bes'4\f\origLineBreak
  bes'2 bes'4
				%120
  bes'2.
  ees'4 f' g'
  g' f' bes'8 bes'
  bes'2 bes'4
  bes'2 bes'4\origLineBreak
				%125
  f'4( g') aes'
  aes' g' bes'8 bes'
  bes'2.
  bes'2 bes'8 bes'
   \autoBeamOn bes'4( a') \autoBeamOff  c''
				%130
   \autoBeamOn c''( bes') \autoBeamOff  d''
   \autoBeamOn d''( c'') \autoBeamOff  bes'\origLineBreak
  bes'2.
  a'2 f'4
  f' bes' d''
				%135
   d''4.( ees''8) \autoBeamOff  d''4
  d'' c'' bes'
  a'4. a'8 bes'4
   \autoBeamOn c''2( bes'4) \autoBeamOff\origLineBreak
  a'2.
				%140
   d''2 \autoBeamOn d''8( c'') \autoBeamOff 
  bes'2 bes'4
   \autoBeamOn ees''4.( d''8 c'' bes'
  a'2) \autoBeamOff  bes'4
   \autoBeamOn bes'2( a'4) \autoBeamOff 
				%145
  bes'2. \bar "|."}


AltoMusic = {
  f'4\f f'8 f' f'4 f'
  f'2 f'
  f'4 f'8 f' f'4 f'
  f'2 f'4 f'
				%5
  f' f' f' f'8 f'
   \autoBeamOn f'4( e') \autoBeamOff  f' c'8 c'
  f'4. f'8 f'2
  f'4. f'8 f'4 r
  f' f' g' g'
				%10
  f'4. ees'8 d'2
  f'4. f'8 e'4 g'
   g' \autoBeamOn f'8( e') \autoBeamOff  f'2
  c' c'
  f' e'
				%15
  f'4 f'8 f' f'4 f'
  e'4. g'8 g'8. g'16 f'4
  f'4. f'8 f'4 f'
  f'2 f'
  f'4 f' f' f'8 f'
				%20
  f'4. f'8 f'2
  f' d'4 d'
   \autoBeamOn d'( f') \autoBeamOff  e'2
  f' d'4 d'
  d' f' e'2
				%25
   a'4 \autoBeamOn f'2( a'4) \autoBeamOff 
  bes' bes'2 f'4
   f' f' \autoBeamOn f'( e') \autoBeamOff 
  f'1
  f'4. f'8 f'4 f'
				%30
  f' f' f' f'
  f' f' ees' d'
  d' c' f' f'
  f'8. f'16 f'4 f' e'
  f'2 r4 c'
				%35
  d'2 ees'4 d'
  d'4. d'8 c'4 f'
  f' f' f' f'8 f'
  f'2 f'4 f'\f
  f'2 f'4 f'
				%40
  g'2 g'4 g'
  g'2 g'4 g'
  f'2 g'4 g'
   f'2.( ees'4) \autoBeamOff 
  d'1
				%45
  \key f \major R1*12
  c'2\mf f'
   \autoBeamOn f'4( e') \autoBeamOff  f'2
  d'4. d'8 c'4 f'
				%60
   \autoBeamOn f'( e') \autoBeamOff  f' f'
  e'4. e'8 f'4 f'
   \autoBeamOn f'2( e') \autoBeamOff 
  f'1
   a'4.(\p g'8) \autoBeamOff  f'2
				%65
  s1*4/4          c'2\pp d'
  d' c'
  c'1
  \key bes \major \time 3/4  d'4\f ees' d'
				%70
  f'2 f'4
  f'4. f'8 f'4
  f' f' f'
  g' g' g'
  f' f' c'8 c'
				%75
  f'4 f' f'
  f'2 f'4
  f' f' f'
  f'4. f'8 f'4
  f'2.
				%80
  f'2 f'4
  f'8. f'16 f'4 f'
  d'8. d'16 d'4 e'
  f' f'8 f' f' f'
  f'2 f'4
				%85
  e' e' g'
  g'8. f'16 f'4 f'8 f'
  f'4 f' f'
   f'2 \autoBeamOn a'8( g') \autoBeamOff 
  f'2.
				%90
  e'2 e'8\p e'
  f'4 e' f'
  g'2 f'4
  f'2 f'4
  e'2 e'4
				%95
  f' e' f'
  g' f' f'8 f'
  f'4 f' f'8 f'
   f'4 f' \autoBeamOn a'8( g') \autoBeamOff 
   \autoBeamOn f'2( e'4) \autoBeamOff 
				%100
  f'2 c'8\f c'
  f'4 f' f'
  f'8. f'16 f'4 f'8 f'
  f'2 e'4
  e' f' c'8 c'
				%105
  f'4 f' f'
  e' f' c'8 c'
  f'2 f'4
  g' f' ees'
   \autoBeamOn f'2( ees'4) \autoBeamOff 
				%110
  d'2.
  ees'4\p ees' ees'
  ees' ees' aes'
  g'2 f'4
  ees'2.
				%115
  ees'4 ees' ees'
   ees'8. ees'16 ees'4 ees'
  d'( ees') f'
  ees'2 f'4\f
  g'2 g'4
				%120
  f'2.
  ees'4 d' ees'
  ees' d' f'8 f'
  g'2 g'4
  aes'2 g'4
				%125
   \autoBeamOn d'( ees') \autoBeamOff  f'
  f' ees' f'8 f'
  g'2.
  f'2 f'8 f'
  ees'2 ees'4
				%130
  f'2 f'4
  g'2 g'4
  f'2.
  f'2 c'4
  f' f' f'
				%135
  f'2 f'4
  g' g' g'
  f'4. f'8 f'4
  f'2.
  f'
				%140
  f'2 f'4
   g'2 \autoBeamOn g'8( f') \autoBeamOff 
    ees'2( g'4
   f'4. ees'8)  \autoBeamOn d'( ees') \autoBeamOff 
   \autoBeamOn f'2( ees'4) \autoBeamOff 
				%145
  d'2. \bar "|."
}


TenorMusic = {
  d'4\f d'8 d' ees'4 d'
  d'2 c'
  bes4 bes8 bes c'4 bes
  bes2 a4 a
				%5
  bes bes ees' d'8 d'
  c'4.( bes8) a4 a8 a
   bes4. bes8 \autoBeamOn bes4( d') \autoBeamOff 
  c'4. c'8 c'4 a~ 
  a bes bes bes
				%10
   \autoBeamOn bes( a8.)  \autoBeamOff a16\noBeam bes2
  bes4. d'8 c'4 c'
  c'4. c'8 c'2
   \autoBeamOn a4( c') \autoBeamOff \autoBeamOn bes( a) \autoBeamOff 
  c'2 c'
				%15
  c'4 c'8 c' bes4 bes
  g c' c'8. c'16 c'4
  a4. a8 a4 a
  bes2 a
  c'4 c' c' c'8 c'
				%20
  d'4. d'8 c'2
  R1
  c'2 \autoBeamOn c'8( bes) \autoBeamOff \autoBeamOn a( g) \autoBeamOff 
   \autoBeamOn f4.( g16 a) \autoBeamOff  g2
   \autoBeamOn a4( c') \autoBeamOff \autoBeamOn c'8( bes) \autoBeamOff \autoBeamOn a( g) \autoBeamOff 
				%25
  f4. a8 c'4 r
  r2 r4 d'
  c'2 c'4.( bes8)
  a2 a
  bes4. bes8 bes4 ees'
				%30
  d' bes a bes
  c' bes a bes
  f f bes bes
  ees'8. ees'16 d'4 c'4. c'8
  c'2 r4 f
				%35
  bes2 a4 bes
  f4. f8 f4 a
  bes bes c' bes8 bes
  bes2 a4 a\f
  bes2 bes4 bes
				%40
  bes2 bes4 bes
  c' c'2 c'4
  a2 bes4 ees'
   \autoBeamOn d'2( c') \autoBeamOff 
  bes1
				%45
  \key f \major R1*12
  a2\mf a
   \autoBeamOn d'4( c') \autoBeamOff  c'2
  r r4 c'
				%60
  c'2 c'4 c'
  c'4. c'8 c'4 c'
   \autoBeamOn d'2( c'4 bes) \autoBeamOff 
  a1
  f'2\p f
				%65
   c'4.( bes8) \autoBeamOff  a2
  a\pp a
   g \autoBeamOn g4( a8 bes) \autoBeamOff 
  a1
  \key bes \major \time 3/4  bes4\f bes bes
				%70
  d'2 bes4
  c'4. c'8 bes4
  bes a d'
  d' d' ees'
  d' c' a8 a
				%75
  bes4 bes bes
   bes4.( c'8) \autoBeamOff  bes4
  bes a bes
  c'4. c'8 bes4
   \autoBeamOn ees'2( d'4) \autoBeamOff 
				%80
  c'2 a4
  a8. a16 a4 a
  g8. g16 g4 c'
  c' c'8 c' c' c'
  d'2 d'4
				%85
  c' c' c'
  c'8. c'16 c'4 a8 a
  a4 c' a
  bes2 d'4
  c'2.
				%90
  c'2 c'8\p c'
  c'4 c' c'
  c'2 c'4
   \autoBeamOn c'( d') \autoBeamOff  d'
  g2 g4
				%95
  c' c' c'
  c' c' a8 a
  a4 c' a8 a
  bes4 bes d'
   c'2~([ c'8 bes)]
				%100
  a2 a8\f a
  bes4 bes ees'
  d'8. d'16 bes4 bes8 bes
   \autoBeamOn ees'4( d') \autoBeamOff  c'
  c' a a8 a
				%105
  bes4 ees' d'
  c' a a8 a
  bes2 bes4
  c' b c'
   \autoBeamOn d'2( c'4) \autoBeamOff 
				%110
  bes2.
  g4\p g bes
  c' c' ees'
  ees'2 d'4
  bes2.
				%115
  g4 g bes
  c'8. c'16 c'4 c'
  bes2 bes4
  bes2 d'4\f
  ees'2 ees'4
				%120
  d'2.
  bes4 bes bes
  bes bes d'8 d'
  ees'2 ees'4
  f'2 ees'4
				%125
  bes2 bes4
  bes bes d'8 d'
  ees'2.
  d'2 d'8 d'
  c'2 a4
				%130
  f2 bes4
  g2 ees'4
  d'2.
  c'2 a4
  bes bes bes
				%135
   bes4.( c'8) \autoBeamOff  bes4
   bes ees' \autoBeamOn ees'8( d') \autoBeamOff 
  c'4. c'8 d'4
   \autoBeamOn ees'2( d'4) \autoBeamOff 
  c'2.
				%140
  d'2 d'4
  d'2 bes4
   \autoBeamOn c'2( ees'4
  c'2) \autoBeamOff  bes4
  c'2.
				%145
  bes \bar "|."
}


BassMusic = {
  bes4\f bes8 bes a4 bes
  f2 f
  bes4 bes8 bes a4 bes
  f2 f4 f
				%5
  bes, d c bes,8 bes,
  c2 f4 f8 ees
  d2 d4 bes,
  f4. f8 f4 r
  f bes ees ees
				%10
  f4. f8 bes,2
  bes,4. bes,8 c4 c
  c4. c8 f2
   \autoBeamOn f4( a) \autoBeamOff \autoBeamOn g( f) \autoBeamOff 
  c'2 c
				%15
  f4 ees8 ees d4 bes,
  c4. c8 f8. f16 f4
  f4. f8 f4 f
  bes,2 f
  f4 f f f8 f
				%20
  bes,4. bes,8 f2
  a bes4 bes
  c'2 c
  f bes,4 bes,
  c4. c8 c2
				%25
  f f
  bes bes4 bes
  c'2 c
  f f
  d4. d8 d4 c
				%30
  bes, bes, f f
  f f f f
  f f bes, d
  c8. c16 bes,4 c4. c8
  f2 r
				%35
  R1
  r2 r4 f
  bes bes a bes8 bes
  f2 f4 r
  r2 r4 bes,\f
				%40
  ees2 ees4 ees
  c2 c4 c
   \autoBeamOn f( d) \autoBeamOff  g ees
  f1
  bes,
				%45
  \key f \major R1*12
  f2\mf d
   \autoBeamOn bes,4( c) \autoBeamOff  f2
  bes4. bes8 a4 a
				%60
  g2 f4 f
  c4. c8 f4 f
   \autoBeamOn bes,2( c) \autoBeamOff 
  f1
  s1*4/4 %65
  f2\p f,
  f\pp d
  bes, c
  f1
  \key bes \major \time 3/4  bes,4\f bes, bes,
				%70
  bes2 bes4
  a4. a8 bes4
  f f d
  g g ees
  f f f8 ees
				%75
  d4 d bes,
  bes,2 bes,4
  bes, c d
  ees4. ees8 d4
   \autoBeamOn a2( bes4) \autoBeamOff 
				%80
  f2 f4
  f8. f16 f4 d
  g8. g16 g4 c
  f f8 f f f
  bes2 bes4
				%85
  c' c' c
  f8. f16 f4 f8 f
  f4 f f
  bes2 bes4
  c'2.
				%90
  c2 c'8\p bes
  a4 g f
  e2 f4
   \autoBeamOn f( d) \autoBeamOff  b,
  c2 c4
				%95
  a g f
  e f f8 f
  f4 f f8 f
  bes,4 bes, bes,
  c2.
				%100
  f2 f8\f ees
  d4 d c
  bes,8. bes,16 bes,4 d8 d
   \autoBeamOn c4( bes,) \autoBeamOff  c
  c f f8 ees
				%105
  d4 c bes,
  c f f8 ees
  d2 bes,4
  ees d c
  f2.
				%110
  bes,
  ees4\p ees ees
  aes aes aes
  bes2 bes,4
  ees2.
				%115
  ees4 ees ees
  aes8. aes16 aes4 aes
  bes2 bes,4
  ees2 bes,4\f
  ees2 ees4
				%120
  bes,2.
  g4 f ees
  bes bes, bes,8 bes,
  ees2 ees4
  d2 ees4
				%125
  bes,2 bes,4
  ees ees bes,8 bes,
  ees2.
  bes,2 bes,8 bes,
  c2 c4
				%130
  d2 d4
  ees2 ees4
  f2.
  f2 f4
  d d bes,
				%135
  bes,2 bes,4
  ees ees ees
  f4. f8 bes4
   \autoBeamOn a2( bes4) \autoBeamOff 
  f2.
				%140
  d2 d4
  g2 g4
   c2.(
  f2) \autoBeamOff  bes,4
  f2.
				%145
  bes, \bar "|."
}

TrebleLyrics = \lyricmode {
  Cre -- do in u -- num De -- um Pa -- trem om -- ni -- po -- ten -- tem, fac --
  to -- rem cœ -- li et ter -- ræ vi -- si -- bi -- li -- um om -- ni -- um et -- in -- vi si --
  bi -- li -- um Et in u -- num Do -- mi -- num Je -- sum Chris -- tum
  fi -- li -- um De -- i u -- ni -- ge -- ni -- tum, Et ex Pa -- tre na -- tum an -- te om -- ni -- a
  sæ -- cu -- la De -- um de De -- o lu -- men de lu -- mi -- ne
  De -- um ve -- rum de De -- o ve -- ro Ge -- ni -- tum non
  fac -- tum con -- sub -- stan -- ti -- a -- lem Pa -- tri per -- quem om -- ni -- a fac -- ta sunt Qui
  prop -- ter nos ho -- mi -- nes et prop -- ter nos -- tram sa -- lu -- tem des -- cen -- dit de
  cœ -- lis des -- cen -- dit des -- cen -- dit de cœ -- lis.
  Et in -- car -- na -- tus est de Spi -- ri -- tu sanc -- to ex Ma -- ri -- a Vir -- gi -- ne
  ex Ma -- ri -- a Vir -- gi -- ne et ho -- mo et ho -- mo et ho -- mo fac -- tus est.
  Cru -- ci -- fix -- us e -- ti -- am pro no -- bis sub Pon -- ti -- o Pi -- la --
  to pas -- sus pas -- sus et se -- pul -- tus est.
  Et re -- sur -- rex -- it ter -- ti -- a di -- e se -- cun -- dum scrip -- tu -- ras Et as --
  cen -- dit in cœ -- lum se -- dit ad dex -- te -- ram Pa -- tris.
  Et i -- ter -- um ven -- tu -- rus est cum glo -- ri -- a ju -- di -- ca -- re vi -- vos et mor -- tu -- os cu -- jus
  reg -- ni non e -- rit fi -- nis.  Et in Spi -- ri -- tum san -- tum Do -- mi --
  num et vi -- vi -- fi -- can -- tem qui ex Pa -- tre Fi -- li -- o -- que pro -- ce --
  dit Qui cum Pa -- tre et Fi -- li -- o si -- mul a -- do -- ra -- tur et con -- glor -- ri -- fi --
  ca -- tur qui lo -- cu -- tus est per Pro -- phe -- tas.  Et u -- nam sanct -- am ca --
  tho -- lic -- cam et A -- pos -- to -- li cam Ec -- cle -- si -- am.  Con --
  fi -- te -- or u -- num bap -- tis -- ma in re -- mis -- si -- o -- nem
  pec -- ca -- to -- rum; Et ex -- pec -- to re -- sur -- rec -- ti -- o -- nem mor -- tu
  o -- rum; Et vi -- tam ven -- tu ri ven -- tu -- ri sæ -- cu -- li A --
  men A -- men A -- men A -- men A --  men.
}

AltoLyrics = \lyricmode {
  Cre -- do in u -- num De -- um Pa -- trem om -- ni -- po -- ten -- tem, fac --
  to -- rem cœ -- li et ter -- ræ vi -- si -- bi -- li -- um om -- ni -- um et -- in -- vi si --
  bi -- li -- um Et in u -- num Do -- mi -- num Je -- sum Chris -- tum
  fi -- li -- um De -- i u -- ni -- ge -- ni -- tum, Et ex Pa -- tre na -- tum an -- te om -- ni -- a
  sæ -- cu -- la De -- um de De -- o lu -- men de lu -- mi -- ne
  De -- um ve -- rum de De -- o ve -- ro Ge -- ni -- tum non
  fac -- tum con -- sub -- stan -- ti -- a -- lem Pa -- tri per -- quem om -- ni -- a fac -- ta sunt Qui
  prop -- ter nos ho -- mi -- nes et prop -- ter nos -- tram sa -- lu -- tem des -- cen -- dit de
  cœ -- lis des -- cen -- dit des -- cen -- dit de cœ -- lis.
				% no 'et incarnatus'
  Cru -- ci -- fix -- us e -- ti -- am pro no -- bis sub Pon -- ti -- o Pi -- la --
  to pas -- sus  et se -- pul -- tus est.
  Et re -- sur -- rex -- it ter -- ti -- a di -- e se -- cun -- dum scrip -- tu -- ras Et as --
  cen -- dit in cœ -- lum se -- dit ad dex -- te -- ram Pa -- tris.
  Et i -- ter -- um ven -- tu -- rus est cum glo -- ri -- a ju -- di -- ca -- re vi -- vos et mor -- tu -- os cu -- jus
  reg -- ni non e -- rit fi -- nis.  Et in Spi -- ri -- tum san -- tum Do -- mi --
  num et vi -- vi -- fi -- can -- tem qui ex Pa -- tre Fi -- li -- o -- que pro -- ce --
  dit Qui cum Pa -- tre et Fi -- li -- o si -- mul a -- do -- ra -- tur et con -- glor -- ri -- fi --
  ca -- tur qui lo -- cu -- tus est per Pro -- phe -- tas.  Et u -- nam sanct -- am ca --
  tho -- lic -- cam et A -- pos -- to -- li cam Ec -- cle -- si -- am.  Con --
  fi -- te -- or u -- num bap -- tis -- ma in re -- mis -- si -- o -- nem
  pec -- ca -- to -- rum; Et ex -- pec -- to re -- sur -- rec -- ti -- o -- nem mor -- tu
  o -- rum; Et vi -- tam ven -- tu ri ven -- tu -- ri sæ -- cu -- li A --
  men A -- men A -- men A -- men A --  men.
}

TenorLyrics = \lyricmode {
  Cre -- do in u -- num De -- um Pa -- trem om -- ni -- po -- ten -- tem, fac --
  to -- rem cœ -- li et ter -- ræ vi -- si -- bi -- li -- um om -- ni -- um et -- in -- vi si --
  bi -- li -- um Et in u -- num Do -- mi -- num Je -- sum Chris -- tum
  fi -- li -- um De -- i u -- ni -- ge -- ni -- tum, Et ex Pa -- tre na -- tum an -- te om -- ni -- a
  sæ -- cu -- la De -- um de De -- o lu -- men de lu -- mi -- ne
  de De -- o ve -- ro Ge -- ni -- tum non
  fac -- tum con -- sub -- stan -- ti -- a -- lem Pa -- tri per -- quem om -- ni -- a fac -- ta sunt Qui
  prop -- ter nos ho -- mi -- nes et prop -- ter nos -- tram sa -- lu -- tem des -- cen -- dit de
  cœ -- lis des -- cen -- dit des -- cen -- dit de cœ -- lis.
				% no 'et incarnatus'
  Cru -- ci -- fix -- us  pro no -- bis sub Pon -- ti -- o Pi -- la
  to pas -- sus  pas -- sus et se -- pul -- tus est.
  Et re -- sur -- rex -- it ter -- ti -- a di -- e se -- cun -- dum scrip -- tu -- ras Et as --
  cen -- dit in cœ -- lum se -- dit ad dex -- te -- ram Pa -- tris.
  Et i -- ter -- um ven -- tu -- rus est cum glo -- ri -- a ju -- di -- ca -- re vi -- vos et mor -- tu -- os cu -- jus
  reg -- ni non e -- rit fi -- nis.  Et in Spi -- ri -- tum san -- tum Do -- mi --
  num et vi -- vi -- fi -- can -- tem qui ex Pa -- tre Fi -- li -- o -- que pro -- ce --
  dit Qui cum Pa -- tre et Fi -- li -- o si -- mul a -- do -- ra -- tur et con -- glor -- ri -- fi --
  ca -- tur qui lo -- cu -- tus est per Pro -- phe -- tas.  Et u -- nam sanct -- am ca --
  tho -- lic -- cam et A -- pos -- to -- li cam Ec -- cle -- si -- am.  Con --
  fi -- te -- or u -- num bap -- tis -- ma in re -- mis -- si -- o -- nem
  pec -- ca -- to -- rum; Et ex -- pec -- to re -- sur -- rec -- ti -- o -- nem mor -- tu
  o -- rum; Et vi -- tam ven -- tu ri ven -- tu -- ri sæ -- cu -- li A --
  men A -- men A -- men A -- men A --  men.
}

BassLyrics = \lyricmode {
  Cre -- do in u -- num De -- um Pa -- trem om -- ni -- po -- ten -- tem, fac --
  to -- rem cœ -- li et ter -- ræ vi -- si -- bi -- li -- um om -- ni -- um et -- in -- vi si --
  bi -- li -- um Et in u -- num Do -- mi -- num Je -- sum Chris -- tum
  fi -- li -- um De -- i u -- ni -- ge -- ni -- tum, Et ex Pa -- tre na -- tum an -- te om -- ni -- a
  sæ -- cu -- la De -- um de De -- o lu -- men de lu -- mi -- ne
  De -- um ve -- rum de De -- o ve -- ro Ge -- ni -- tum non
  fac -- tum con -- sub -- stan -- ti -- a -- lem Pa -- tri per -- quem om -- ni -- a fac -- ta sunt
  et prop -- ter nos -- tram sa -- lu -- tem des -- cen -- dit de
  cœ -- lis des -- cen -- dit  de cœ -- lis.
				% no 'et incarnatus'
  Cru -- ci -- fix -- us  e -- ti -- am pro no -- bis sub Pon -- ti -- o Pi -- la
  to pas -- sus  et se -- pul -- tus est.
  Et re -- sur -- rex -- it ter -- ti -- a di -- e se -- cun -- dum scrip -- tu -- ras Et as --
  cen -- dit in cœ -- lum se -- dit ad dex -- te -- ram Pa -- tris.
  Et i -- ter -- um ven -- tu -- rus est cum glo -- ri -- a ju -- di -- ca -- re vi -- vos et mor -- tu -- os cu -- jus
  reg -- ni non e -- rit fi -- nis.  Et in Spi -- ri -- tum san -- tum Do -- mi --
  num et vi -- vi -- fi -- can -- tem qui ex Pa -- tre Fi -- li -- o -- que pro -- ce --
  dit Qui cum Pa -- tre et Fi -- li -- o si -- mul a -- do -- ra -- tur et con -- glor -- ri -- fi --
  ca -- tur qui lo -- cu -- tus est per Pro -- phe -- tas.  Et u -- nam sanct -- am ca --
  tho -- lic -- cam et A -- pos -- to -- li cam Ec -- cle -- si -- am.  Con --
  fi -- te -- or u -- num bap -- tis -- ma in re -- mis -- si -- o -- nem
  pec -- ca -- to -- rum; Et ex -- pec -- to re -- sur -- rec -- ti -- o -- nem mor -- tu
  o -- rum; Et vi -- tam ven -- tu ri ven -- tu -- ri sæ -- cu -- li A --
  men A -- men A -- men A -- men A --  men.
}


initialKeyTime = {\autoBeamOff \key bes \major \time 4/4}
pieceTitle = "Credo"

\include "template.ly"