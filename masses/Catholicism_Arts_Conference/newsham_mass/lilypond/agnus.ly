\include "dynamics.ly"
				% The music follows

TrebleMusic= {
  \tempo "Largo" 4 = 46
  bes'2\mf d''4
  \autoBeamOn c''8( d'') \autoBeamOff   bes'4 r
  a'2 bes'4
  g' f' r8 f'
				%5
  \autoBeamOn f'( bes') \autoBeamOff   bes'4 bes'\origLineBreak
  \autoBeamOn a'8.( bes'32 c'') \autoBeamOff   bes'4. d''8
  \autoBeamOn d''( g'4  c''8)\noBeam \autoBeamOff  \autoBeamOn c''( d'') \autoBeamOff  
  bes'4 a' r
  \autoBeamOn bes'(\p c'') \autoBeamOff   d''
				%10
  \autoBeamOn ees''( d'') \autoBeamOff   c''
  c'' bes' r\origLineBreak           \autoBeamOn bes'4( c'') \autoBeamOff   bes'
  \autoBeamOn bes'( aes') \autoBeamOff   g'
  g' f' r
				%15
  \autoBeamOn g'(\f bes') \autoBeamOff   \autoBeamOn ees''8( bes') \autoBeamOff  
  b'4( c'') aes'
  \autoBeamOn g'2( f'4) \autoBeamOff  
  ees'2. \bar "||"\origLineBreak\origLineBreak
  bes'2\p bes'4
				%20
  \autoBeamOn aes'8( g') \autoBeamOff   g'4. g'8
  \autoBeamOn g'( f') \autoBeamOff   f'4 f'
  \autoBeamOn f'8( ees')\autoBeamOff ees'4 r8 bes'
  \autoBeamOn bes'( ees'') \autoBeamOff   ees''4. g'8
  \autoBeamOn g'( aes') \autoBeamOff   g'4. ees''8\origLineBreak
				%25
  \autoBeamOn ees''( d''4 c'') \autoBeamOff   bes'8
  \autoBeamOn bes'2( aes'4) \autoBeamOff  
  g'2 r4
  \autoBeamOn ees''(\f d'') \autoBeamOff   c''
  b' c'' r
				%30
  g'2\p c''4
  a' bes' r\origLineBreak
  \autoBeamOn f'4(\f bes') \autoBeamOff   d''
  \autoBeamOn d''( ees'') \autoBeamOff   c''
  \autoBeamOn bes'2( a'4) \autoBeamOff  
				%35
  bes'2. \bar "||"         bes'2 d''4\p
  \autoBeamOn c''8( d'') \autoBeamOff   bes'4 r
  a'2 bes'4\origLineBreak
  g' f' r8 f'\f
				%40
  \autoBeamOn f'( bes') \autoBeamOff   bes'4 bes'
  \autoBeamOn a'8.( bes'32 c'') \autoBeamOff   bes'4. d''8
  \autoBeamOn d''( g'4 c''8) \autoBeamOff   \autoBeamOn c''([ d'')] \autoBeamOff  
  bes'4 a' r
  \autoBeamOn bes'(\p c'') \autoBeamOff   d''\origLineBreak
				%45
  \autoBeamOn d''( c'') \autoBeamOff   bes'
  bes' a' r
  \autoBeamOn d''( c'') \autoBeamOff   bes'
  \autoBeamOn a'( bes') \autoBeamOff   g'
  g' f' r
				%50
  c''2 ees''4
  ees''2 d''4\origLineBreak
  a'2 c''4
  c'' bes' r
  \autoBeamOn f'( bes') \autoBeamOff   d''
				%55
  \autoBeamOn d''( ees'') \autoBeamOff   c''
  \autoBeamOn bes'2.(
  a') \autoBeamOff  
  bes' \bar "||"}


AltoMusic= {
  f'2\mf f'4
  ees' d' r
  f'2 f'4
  f'8( e') f'4 r8 d'
				%5
  d'4 f' f'
  f' f'4. f'8
  g'2 g'4
  f' f' r
  \autoBeamOn d'(\p ees') \autoBeamOff   f'
				%10
  \autoBeamOn g'( f') \autoBeamOff   ees'
  ees' d' r
  ees'2 g'4
  \autoBeamOn g'( f') \autoBeamOff   ees'
  ees' d' r
				%15
  ees'2\f ees'4
  ees'2 f'4
  \autoBeamOn ees'2( d'4) \autoBeamOff  
  ees'2.
  g'2\p f'4
				%20
  ees' ees'4. ees'8
  c'4 c' d'
  \autoBeamOn d'8( ees') \autoBeamOff   ees'4 r8 ees'
  ees'4 ees'4. ees'8
  d'4 ees'4. ees'8
				%25
  ees'2 ees'4
  \autoBeamOn d'2( f'4) \autoBeamOff  
  ees'2 r4
  \autoBeamOn g'(\f f') \autoBeamOff   ees'
  d' ees' r
				%30
  ees'2\p g'4
  \autoBeamOn f'8( ees') \autoBeamOff   d'4 f'~\f 
  f'2 f'4
  g'2 g'4
  \autoBeamOn f'2( ees'4) \autoBeamOff  
				%35
  d'2.
  f'2\p f'4
  ees' d' r
  f'2 f'4
  \autoBeamOn f'8( e') \autoBeamOff   f'4 f'\f
				%40
  f' f' f'
  f' f' f'
  g'2 g'4
  f' f' r
  \autoBeamOn d'(\p ees') \autoBeamOff   f'
				%45
  \autoBeamOn f'( ees') \autoBeamOff   d'
  d' c' r
  f'2  \autoBeamOn f'8( e') \autoBeamOff  
  f'2 e'4
  e' f' r
				%50
  f'2 f'4
  f'2 f'4
  f'2 f'4
  f' f' f'~ 
  f'2 f'4
				%55
  g'2 g'4
  f'2.~ (
  f'2 ees'4) \autoBeamOff  
  d'2. \bar "|."
}


TenorMusic= {
  d'2\mf bes4
  a bes r
  c'2 d'4
  d' a r8 bes
				%5
  bes4 bes d'
  \autoBeamOn c'8.( d'32 ees') \autoBeamOff   d'4. bes8
  \autoBeamOn bes4( c') \autoBeamOff   ees'
  d' c' r
  bes2\p bes4
				%10
  bes2 bes4
  a bes r
  \autoBeamOn g( aes) \autoBeamOff   bes
  c'2 c'4
  bes bes r
				%15
  \autoBeamOn bes(\f g) \autoBeamOff   g
  g( aes) c'
  \autoBeamOn bes2~( bes8 aes) \autoBeamOff  
  g2.
  ees'2\p d'4
				%20
  \autoBeamOn c'8( bes) \autoBeamOff   bes4 bes
  aes aes aes
  \autoBeamOn aes8( g) \autoBeamOff   g4 r8 g
  g4 g4. bes8
  bes4 bes4. g8
				%25
  \autoBeamOn c'( bes4 aes) \autoBeamOff   g8
  \autoBeamOn f2( bes4) \autoBeamOff  
  bes2 r4
  g2\f g4
  g g r
				%30
  bes2\p ees'4
  c' bes r
  bes2\f bes4
  \autoBeamOn b( c') \autoBeamOff   ees'
  \autoBeamOn d'2( c'4) \autoBeamOff  
				%35
  bes2.
  d'2\p bes4
  a bes r
  c'2 d'4
  c' a r8 d'\f
				%40
  d'4 bes d'
  \autoBeamOn c'8.( d'32 ees') \autoBeamOff   d'4. bes8
  \autoBeamOn bes4( c') \autoBeamOff   ees'
  d' c' r
  R1*3/4
				%45
  R1*3/4
  R1*3/4
  \autoBeamOn bes4(^\boxP c') \autoBeamOff   c'
  \autoBeamOn c'( d') \autoBeamOff   c'
  bes a r
				%50
  a2 c'4
  c'2 bes4
  c'2 ees'4
  ees' d' r
  bes2 bes4
				%55
  \autoBeamOn b( c') \autoBeamOff   ees'
  \autoBeamOn d'2.(
  c') \autoBeamOff  
  bes \bar "|."
}


BassMusic= {
  bes2\mf bes4
  f bes, r
  f2 bes,4
  c f r8 bes,
				%5
  bes,4 d bes,
  f bes, bes,
  ees2 ees4
  f f r
  s1*3/4
				%10
  s1*3/4
  s1*3/4
  ees2^\boxP ees4
  aes2 a4
  bes bes, r
				%15
  ees2\f ees4
  aes,2 aes,4
  bes,2.
  ees
  s1*3/4
				%20
  s1*3/4
  r4 r bes,^\boxP
  ees ees ees
  ees ees ees
  bes, ees4. ees8
				%25
  aes,2 aes,4
  bes,2.
  ees2 r4
  c2\f c4
  g c r
				%30
  \autoBeamOn ees(\p d) \autoBeamOff   c
  f bes, r
  d2\f bes,4
  ees2 ees4
  f2.
				%35
  bes,
  bes2\p bes4
  f bes, r
  f2 bes,4
  c f r8 bes,\f
				%40
  bes,4 d bes,
  f bes, bes,
  ees2 ees4
  f f r
  bes,2\p bes,4
				%45
  ees2 e4
  f f r
  \autoBeamOn bes( a) \autoBeamOff   g
  \autoBeamOn f( bes,) \autoBeamOff   c
  c f f~ 
				%50
  f2 f4
  f2 f4
  f2 a4
  a bes r
  d2 bes,4
				%55
  ees2 ees4
  f2.~ 
  f
  bes, \bar "|."
}

TrebleLyrics = \lyricmode {
  Ag -- nus De -- i Ag -- nus De -- i qui tol -- lis pec --
  ca -- ta pec -- ca -- ta mun -- di mi -- se -- re -- re no -- bis
  mi -- se -- re -- re no -- bis mi -- se -- re -- re no -- bis.
  Ag -- nus De -- i qui tol -- lis pec -- ca -- ta qui tol -- lis pec -- ca -- ta pec --
  ca -- ta mun -- di mi -- se -- re -- re mi -- se -- re -- re
  mi -- se -- re -- re no -- bis. Ag -- nus De -- i Ag -- nus
  De -- i qui tol -- lis pec -- ca -- ta pec -- ca -- ta mun -- di Do -- na
  no -- bis pa -- cem do -- na no -- bis pa -- cem do -- na no -- bis
  pa -- cem pa -- cem do -- na no -- bis pa -- cem.
}

AltoLyrics = \lyricmode {
  Ag -- nus De -- i Ag -- nus De -- i qui tol -- lis pec --
  ca -- ta pec -- ca -- ta mun -- di mi -- se -- re -- re no -- bis
  mi -- se -- re -- re no -- bis mi -- se -- re -- re no -- bis.
  Ag -- nus De -- i qui tol -- lis pec -- ca -- ta qui tol -- lis pec -- ca -- ta pec --
  ca -- ta mun -- di mi -- se -- re -- re mi -- se -- re -- re
  mi -- se -- re -- re no -- bis. Ag -- nus De -- i Ag -- nus
  De -- i qui tol -- lis pec -- ca -- ta pec -- ca -- ta mun -- di Do -- na
  no -- bis pa -- cem do -- na no -- bis pa -- cem do -- na no -- bis
  pa -- cem pa -- cem do -- na no -- bis pa -- cem.
}

TenorLyrics = \lyricmode {
  Ag -- nus De -- i Ag -- nus De -- i qui tol -- lis pec --
  ca -- ta pec -- ca -- ta mun -- di mi -- se -- re -- re no -- bis
  mi -- se -- re -- re no -- bis mi -- se -- re -- re no -- bis.
  Ag -- nus De -- i qui tol -- lis pec -- ca -- ta qui tol -- lis pec -- ca -- ta pec --
  ca -- ta mun -- di mi -- se -- re -- re mi -- se -- re -- re
  mi -- se -- re -- re no -- bis. Ag -- nus De -- i Ag -- nus
  De -- i qui tol -- lis pec -- ca -- ta pec -- ca -- ta mun -- di Do -- na
  no -- bis pa -- cem do -- na no -- bis pa -- cem pa -- cem do -- na no -- bis pa -- cem.
}

BassLyrics = \lyricmode {
  Ag -- nus De -- i Ag -- nus De -- i qui tol -- lis pec --
  ca -- ta pec -- ca -- ta mun -- di 
  mi -- se -- re -- re no -- bis mi -- se -- re -- re no -- bis.
  pec -- ca -- ta qui tol -- lis pec -- ca -- ta pec --
  ca -- ta mun -- di mi -- se -- re -- re mi -- se -- re -- re
  mi -- se -- re -- re no -- bis. Ag -- nus De -- i Ag -- nus
  De -- i qui tol -- lis pec -- ca -- ta pec -- ca -- ta mun -- di Do -- na
  no -- bis pa -- cem do -- na no -- bis pa -- cem do -- na no -- bis
  pa -- cem pa -- cem do -- na no -- bis pa -- cem.

}

initialKeyTime = { \key bes \major \time 3/4}
\include "template.ly"