\include "dynamics.ly"
				% The music follows

TrebleMusic= {
  \tempo "Largo" 4 = 44
  f'2^\boxF \autoBeamOn f'4( bes') \autoBeamOff
  \autoBeamOn bes'( a') \autoBeamOn bes'( d'') \autoBeamOff
  c''1
  d''
  				%5
  c''8. f'16 f'4 bes' a'\origLineBreak
  g'4. g'8 a'2
  bes'4 c''8 d'' ees''4 d''
  \autoBeamOn g'8( a') \autoBeamOff bes' c'' bes'4 a'
  d''4. d''8 c''2
  				%10
  bes' a'4 r8 \tempo "Andante" 4 = 63 c''^\boxMF\origLineBreak
  d''8 bes' a' bes' g' f' r f'
  f' bes' a' bes' ees'' d'' r bes'
  \autoBeamOn bes'( a') \autoBeamOff c''\noBeam c'' \autoBeamOn c''( bes') \autoBeamOff d''\noBeam a'\origLineBreak
  bes'8 bes' c'' c'' d'' f'' ees'' c''
				%15
  \autoBeamOn bes'4( a') bes'2 \bar "||"
  \tempo "Largo" 4 = 40
  \autoBeamOn c''4\p d''8( c'') \autoBeamOn c''( f'') \autoBeamOff f''\noBeam c''
  \autoBeamOn cis''( d'') d''4. g'8\noBeam \autoBeamOn g'( bes') \autoBeamOff\origLineBreak
  a'4 r8 c'' \autoBeamOn c''( f'') \autoBeamOff f''\noBeam a'
  \autoBeamOn bes'2~( bes'8 d'') \autoBeamOff d''\noBeam bes'
  				%20
  a'4. \autoBeamOn bes'16( g') \autoBeamOff f'4 r8 \tempo "Andante" 4 = 63 c''\p
  d'' bes' a' bes' g' f' r f'\origLineBreak
  f'8 bes' a' bes' ees'' d'' r bes'
  \autoBeamOff bes'([ a')] c'' c''  c''([ bes')]  d'' a'
  bes' bes' c'' c'' d'' f'' ees'' c''
  				%25
  \set Timing.measurePosition = #(ly:make-moment -7/8)  \autoBeamOn bes'4( a') \autoBeamOff bes'4.\origLineBreak \bar "||"
  \partial 256*32  c''8\f
  d'' bes' a' bes' g' f' r f'
  f'8 bes' a' bes' ees'' d'' r bes'
  \autoBeamOff bes'([ a')] c'' c''  c''([ bes')]  d'' a'
  				%30
  bes' bes' c'' c'' d'' f'' ees'' c''
  \autoBeamOn bes'4( a') \autoBeamOff bes'2 \bar "|."
}


AltoMusic= {
  d'2^\boxF d'
  f' f'
  \autoBeamOn g'( f') \autoBeamOff
  f'1
				%5
  f'8. f'16 f'4 \autoBeamOn f'8( e') \autoBeamOff f'4
  f' e' f'2
  f'4 f'8 f' f'4 f'
  g' g'8 g' f'4 f'
  f'4. f'8 f'2
				%10
  \autoBeamOn f'4( e') \autoBeamOff f' r8 f'^\boxMF
  f' f' f' f' e' f' r f'
  f' f' f' f' f' f' r f'
  ees'4 ees'8 g' f'4 f'8 f'
  f' f' ees' ees' d' bes' g' ees'
				%15
  \autoBeamOn d'4( f'8 ees') \autoBeamOff d'2
  R1 * 4
				%20
  r2 r4 r8 f'\p
  f' f' f' f' e' f' r f'
  f' f' f' f' f' f' r f'
  ees'4 ees'8 g' f'4 f'8 f'
  f' f' ees' ees' d' bes' g' ees'
				%25
  \set Timing.measurePosition = #(ly:make-moment -7/8)  \autoBeamOn d'4( f'8[ ees')] \autoBeamOff d'4.
  \partial 256*32  f'8\f
  f' f' f' f' e' f' r f'
  f' f' f' f' f' f' r f'
  ees'4 ees'8 g' f'4 f'8 f'
				%30
  f' f' ees' ees' d' bes' g' ees'
  \autoBeamOn d'4( f'8 ees') \autoBeamOff d'2 \bar "|."
}


TenorMusic= {
  bes2^\boxF bes
  c' bes
  \autoBeamOn bes( a) \autoBeamOff
  bes1
				%5
  c'8. c'16 c'4 c' c'
  c'4. c'8 c'2
  bes4 a8 bes c'4 bes
  bes ees'8 ees' d'4 c'
  bes4. bes8 c'2
				%10
  \autoBeamOn d'4( c') \autoBeamOff c' r8 a^\boxMF
  bes bes ees' d' c' a r a
  bes bes ees' d' c' bes r d'
  c'4 c'8 ees' d'4 bes8 c'
  bes bes a a bes bes c' c'
				%15
  \autoBeamOn d'4( c') \autoBeamOff bes2
  R1*4
				%20
  r2 r4 r8 a\p
  bes bes ees' d' c' a r a
  bes bes ees' d' c' bes r d'
  c'4 c'8 ees' d'4 bes8 c'
  bes bes a a bes bes c' c'
				%25
  \set Timing.measurePosition = #(ly:make-moment -7/8)  \autoBeamOn d'4( c') \autoBeamOff bes4.
  \partial 256*32  a8\f
  bes bes ees' d' c' a r a
  bes bes ees' d' c' bes r d'
  c'4 c'8 ees' d'4 bes8 c'
				%30
  bes bes a a bes bes c' c'
  \autoBeamOn d'4( c') \autoBeamOff bes2 \bar "|."
}


BassMusic= {
  bes,2^\boxF \autoBeamOn d4( bes,) \autoBeamOff
  f2 \autoBeamOn d4( bes,) \autoBeamOff
  \autoBeamOn ees2( f) \autoBeamOff
  bes1
				%5
  a8. a16 a4 g f
  c4. c8 f2
  d4 c8 bes, a,4 bes,
  ees c8 c f4 f
  bes4. bes8 a2
				%10
  g f4 r8 f^\boxMF
  bes, d c bes, c f r f
  d d c bes, a, bes, r bes,
  c4 c8 c d4 d8 ees
  d d c c bes, d ees ees
				%15
  f2 bes,
  R1*4
				%20
  r2 r4 r8 f\p
  bes, d c bes, c f r f
  d d c bes, a, bes, r bes,
  c4 c8 c d4 d8 ees
  d d c c bes, d ees ees
				%25
  \set Timing.measurePosition = #(ly:make-moment -7/8) \autoBeamOff  f2 bes,4.
  \partial 256*32  f8\f
  bes, d c bes, c f r f
  d d c bes, a, bes, r bes,
  c4 c8 c d4 d8 ees
				%30
  d d c c bes, d ees ees
  f2 bes, \bar "|."
}

TrebleLyrics = \lyricmode {
  Sanc -- tus Sanc -- tus Sanc -- tus Do -- mi -- nus De -- us
  Sa -- ba -- oth: Ple -- ni sunt -- cœ -- li cœ -- li et  ter -- ra glo -- ri -- a tu -- a Ho --
  san -- na in  ex -- cel -- sis Ho -- san -- na in ex -- cel -- sis Ho -- san -- na Ho -- sa -- na Ho --
  san -- na  in ex -- cel -- sis in ex -- cel sis. Be -- ne -- dic -- tus qui ve -- nit qui ve --
  nit qui ve -- nit in no -- mi -- ne Do -- mi -- ni. Ho -- san -- na in ex -- cel -- sis Ho --
  san -- na in ex -- cel -- sis Ho -- san -- na Ho -- san -- na Ho -- san -- na in ex -- cel -- sis in ex -- cel -- sis.
  Ho -- san -- na in  ex -- cel -- sis Ho --
  san -- na in ex cel -- sis Ho -- san -- na Ho san -- na Ho -- san -- na in ex -- cel -- sis in  ex -- cel -- sis.
}

AltoLyrics = \lyricmode {
  Sanc -- tus Sanc -- tus Sanc -- tus Do -- mi -- nus De -- us
  Sa -- ba -- oth: Ple -- ni sunt -- cœ -- li cœ -- li et  ter -- ra glo -- ri -- a tu -- a Ho --
  san -- na in  ex -- cel -- sis Ho -- san -- na in ex -- cel -- sis Ho -- san -- na Ho -- sa -- na Ho --
  san -- na  in ex -- cel -- sis in ex -- cel sis.
  Ho -- san -- na in ex -- cel -- sis Ho --
  san -- na in ex -- cel -- sis Ho -- san -- na Ho -- san -- na Ho -- san -- na in ex -- cel -- sis in ex -- cel -- sis.
  Ho -- san -- na in  ex -- cel -- sis Ho --
  san -- na in ex cel -- sis Ho -- san -- na Ho san -- na Ho -- san -- na in ex -- cel -- sis in  ex -- cel -- sis.
}

TenorLyrics = \lyricmode {
  Sanc -- tus Sanc -- tus Sanc -- tus Do -- mi -- nus De -- us
  Sa -- ba -- oth: Ple -- ni sunt -- cœ -- li cœ -- li et  ter -- ra glo -- ri -- a tu -- a Ho --
  san -- na in  ex -- cel -- sis Ho -- san -- na in ex -- cel -- sis Ho -- san -- na Ho -- sa -- na Ho --
  san -- na  in ex -- cel -- sis in ex -- cel sis.
  Ho -- san -- na in ex -- cel -- sis Ho --
  san -- na in ex -- cel -- sis Ho -- san -- na Ho -- san -- na Ho -- san -- na in ex -- cel -- sis in ex -- cel -- sis.
  Ho -- san -- na in  ex -- cel -- sis Ho --
  san -- na in ex cel -- sis Ho -- san -- na Ho san -- na Ho -- san -- na in ex -- cel -- sis in  ex -- cel -- sis.
}


BassLyrics = \lyricmode {
  Sanc -- tus Sanc -- tus Sanc -- tus Do -- mi -- nus De -- us
  Sa -- ba -- oth: Ple -- ni sunt -- cœ -- li cœ -- li et  ter -- ra glo -- ri -- a tu -- a Ho --
  san -- na in  ex -- cel -- sis Ho -- san -- na in ex -- cel -- sis Ho -- san -- na Ho -- sa -- na Ho --
  san -- na  in ex -- cel -- sis in ex -- cel sis.
  Ho -- san -- na in ex -- cel -- sis Ho --
  san -- na in ex -- cel -- sis Ho -- san -- na Ho -- san -- na Ho -- san -- na in ex -- cel -- sis in ex -- cel -- sis.
  Ho -- san -- na in  ex -- cel -- sis Ho --
  san -- na in ex cel -- sis Ho -- san -- na Ho san -- na Ho -- san -- na in ex -- cel -- sis in  ex -- cel -- sis.
}


initialKeyTime = {\autoBeamOff \key bes \major \time 4/4}

\include "template.ly"