
TrebleMusic = {
  \tempo "Moderato" 4 = 104
         bes'4.\f bes'8 f'4 f'8 bes'
         d''4.( c''8) bes'2
         bes'4 bes' a' bes'
         g'2 f'\origLineBreak
%5
         bes'4.\p f'8 g'4 a'
         bes' a' g'8. g'16 f'4
         a' bes' ees'' d''
         d''2 c''4 c''\f\origLineBreak
          d''4. c''8 bes'4 a'8 a'
%10
         bes'4 c''8 c'' d''2
         bes'\p \autoBeamOn bes'4( a') \autoBeamOff
         g'4. g'8 f'2
         c''\f c''4 ees''\origLineBreak
         \autoBeamOn d''2( ees''4 d''8 c''
%15
         bes'2) \autoBeamOff a'
         bes'1
         f'4.\p f'8 \autoBeamOn f'4( bes') \autoBeamOff
         bes'4. bes'8 \autoBeamOn bes'4( a') \autoBeamOff
         bes'4. bes'8 \autoBeamOn c''4( d'') \autoBeamOff\origLineBreak
%20
          ees''2 d''
         d''4 c'' bes' a'
         g' a'8 bes' bes'4 a'
         c''\f c''8. c''16 c''4 d''
         bes'4. bes'8 bes'4 c''\origLineBreak
%25
         f' f' bes' bes'8 bes'
         g'4. g'8 f'4 c''~ 
         c'' a'8 a' \autoBeamOn bes'8.( c''16) \autoBeamOff d''4\origNoLineBreak
         \autoBeamOn g'8.( a'16) \autoBeamOff \autoBeamOn bes'8( c'') \autoBeamOff bes'8. bes'16 a'4
         bes'2\p c''
%30
         a' bes'\origLineBreak
          d''4\f d''8 c'' bes'4 b'
         c'' ees'' bes' a'
         d''8. d''16 d''4 bes' bes'
         \autoBeamOn ees''8.( d''16 c''8 bes' a'8.) \autoBeamOff a'16\noBeam bes'4
%35
  \autoBeamOn bes'( a') \autoBeamOff bes'2 \bar "||" \origLineBreak \key ees \major
  \tempo "Largo" 4 = 46
         \partial 256*64  \time 3/4  bes'4\p
         bes' bes' c''
         c'' bes' bes'~ 
         bes' aes' g'
%40
         g' f' r
         \autoBeamOn bes'( aes') \autoBeamOff g'
  \autoBeamOn f'( g' aes'\origLineBreak
  bes'4 aes') \autoBeamOff g'
         g' f' r
%45
         \autoBeamOn ees''( d'') \autoBeamOff c''
         \autoBeamOn b'( c'') \autoBeamOff aes'
         \autoBeamOn g'2( f'4) \autoBeamOff
         ees'2 g'4
         g' c'' b'\origLineBreak
%50
         c'' g' ees''~ 
         ees'' d'' c''
         bes' a' r
         d''4.\mp c''8 bes'4
         a'4. a'8 bes'4
%55
         g'4. g'8 g' g'
         f'2 bes'4\origLineBreak
          \autoBeamOn bes'4( a'8 g' a'4) \autoBeamOff
         bes'2 bes'4\p
         bes' bes' c''
%60
         c''8. c''16 bes'4 bes'~ 
         bes'8. bes'16 \autoBeamOn aes'4( g') \autoBeamOff
         g' f' r
         \autoBeamOn bes'( aes') \autoBeamOff g'\origLineBreak
         \autoBeamOn f'( g' aes'
%65
         bes' aes') \autoBeamOff g'
         g' f' r
         \autoBeamOn ees''( d'') \autoBeamOff c''
         \autoBeamOn b'( c'') \autoBeamOff aes'
         g'2.(
%70
         f') \autoBeamOff
	  g' \bar "||"\origLineBreak
  \time 4/4  \key bes \major
  \tempo "Moderato" 4 = 100
	  bes'4.\f bes'8 f'4 f'
          d''4 c'' c'' bes'8 bes'
         d''4 c'' c''8. c''16 bes'8 bes'
%75
         g'4 bes'8 bes' a'8. a'16 bes'4\origLineBreak
         c''2\p bes'
         bes' a'4\fermata a'\f
         bes' d'' g'8. g'16 g'8 g'
         \autoBeamOn c''( d'' ees'' c'' bes'8.) \autoBeamOff bes'16\noBeam a'8 a'
%80
         bes'4 c''8 c'' d''4 ees''\origLineBreak
          d''2 c''
         d''4 d'' bes' bes'
         \autoBeamOn ees''( c''8 bes' a'4) \autoBeamOff bes'
         \autoBeamOn bes'2( a') \autoBeamOff
%85
         bes'1 \bar "||"}


AltoMusic = {
          f'4.\f f'8 f'4 f'8 f'
         f'4.( ees'8) \autoBeamOff d'2
         f'4 f' f' f'
         \autoBeamOn f'( e') \autoBeamOff f'2
%5
         d'4.\p d'8 ees'4 ees'
         d' f' ees'8. ees'16 d'4
         f' f' f' f'
         f'2 f'4 f'\f
         f'4. ees'8 d'4 f'8 f'
%10
         f'4 ees'8 ees' d'2
         g'\p \autoBeamOn g'4( f') \autoBeamOff
         f' e' f'2
         f'\f f'4 f'
         \autoBeamOn f'2( ees'4 g'
%15
         f'2.) \autoBeamOff ees'4
         d'1
         d'4.\p d'8 d'2
         f'4. f'8 f'2
         f'4. f'8 f'2
%20
         g' f'
         f'4 f' e' f'
         e' f'8 g' g'4 f'
         f'\f f'8. f'16 f'4 f'
         d'4. d'8 d'4 e'
%25
         f' d' d' g'8 f'
         e'4. e'8 f'4 r
         fis' fis'8 fis' \autoBeamOn g'8.( a'16) \autoBeamOff bes'4
         ees' ees' g'8. g'16 fis'4
         g'2\p g'
%30
         \autoBeamOn g'4( fis') \autoBeamOff g'2
         f'4\f f'8 f' g'4 g'
         g' g' f' f'
         f'8. f'16 f'4 g' \autoBeamOn g'8( f') \autoBeamOff
         \autoBeamOn ees'8.( f'16 ees'8 g' c'8.) \autoBeamOff c'16\noBeam \autoBeamOn f'8( g') \autoBeamOff
%35
         f'4.( ees'8) \autoBeamOff d'2 \key ees \major
         \partial 256*64  \time 3/4  ees'4\p
         ees'2.
         ees'2 g'4
         \autoBeamOn g'( f') \autoBeamOff ees'
%40
         ees' d' r
         \autoBeamOn g'( f') \autoBeamOff ees'
         \autoBeamOn d'( ees' f'
         g' f') \autoBeamOff ees'
         ees' d' ees'~ 
%45
         ees'2 ees'4
         ees'2 f'4
         \autoBeamOn ees'2( d'4) \autoBeamOff
         ees'2 ees'4
         ees' ees' f'
%50
         ees' ees' g'~ 
         g' f' ees'
         d' c' r
         f'4.\mp ees'8 d'4
         c'4. c'8 bes4
%55
         ees'4. ees'8 ees' ees'
         \autoBeamOn ees'4( d'8 c') \autoBeamOff d'4
         \autoBeamOn c'( f' ees') \autoBeamOff
         d'2 ees'4\p
         ees' ees' ees'
%60
         ees'8. ees'16 ees'4 g'~ 
         g'8. g'16 \autoBeamOn f'4( ees') \autoBeamOff
         ees' d' r
         \autoBeamOn g'( f') \autoBeamOff ees'
          \autoBeamOn d'4( ees' f'
%65
         g' f') \autoBeamOff ees'
         ees' d' ees'~ 
         ees'2 ees'4
         ees'2 f'4
         ees'2.(
%70
         d') \autoBeamOff
         ees'
         \key bes \major \time 4/4  f'4.\f f'8 f'4 f'
         f' ees' ees' d'8 d'
         f'4 ees' ees'8. ees'16 d'8 d'
%75
         ees'4 d'8 d' f'8. f'16 f'4
         g'2\p g'
         f' f'4\fermata f'\f
         f' f' ees'8. ees'16 ees'8 ees'
         \autoBeamOn ees'( f' g' ees' f'8.) \autoBeamOff f'16\noBeam f'8 f'
%80
         f'4 f'8 f' f'4 g'
         f'2 f'
         f'4 f' ees' ees'
         \autoBeamOn g'2( f'8 ees') \autoBeamOff d'4
         f'2.( ees'4) \autoBeamOff
%85
         d'1 \bar "|."
}


TenorMusic = {
          d'4.\f d'8 d'4 d'8 d'
         bes4.( a8) \autoBeamOff bes2
         bes4 bes ees' d'
         c'4.( bes8) \autoBeamOff a2
%5
         bes4.\p bes8 bes4 f
         bes bes bes8. bes16 bes4
         c' bes a bes
         bes2 a4 a\f
         bes4. c'8 d'4 c'8 c'
%10
         bes4 bes8 a bes2
         d'\p c'
         c'4. bes8 a2
         a\f bes4 c'
         \autoBeamOn bes2.( ees'4
%15
         d' ees'8 d') \autoBeamOff c'2
         bes1
         bes4.\p bes8 bes2
         bes4 d' c'2
         bes a4 bes
%20
         bes2 bes
         bes4 c' c' c'
         c' c'8 c' c'4 c'
         a\f a8. a16 a4 a
         g4. g8 g4 g
%25
         f a g bes8 d'
         c'4. bes8 a4 a~ 
         a d'8 c' \autoBeamOn bes8.( a16) \autoBeamOff g4
         c' ees' d'8. d'16 d'4
         d'2\p ees'
%30
         d' d'
         d'4\f d'8 d' d'4 d'
         c' c' d' c'
         bes8. bes16 bes4 bes bes
         \autoBeamOn g2( f4) \autoBeamOff bes8 bes
%35
         \autoBeamOn d'4( c') \autoBeamOff bes2 \key ees \major
         \partial 256*64  \time 3/4  g4\p
         g g aes
         aes g bes
         c'2 c'4
%40
         bes bes bes~ 
         bes2 bes4
         bes2.(
         bes2) \autoBeamOff \autoBeamOn bes8( c') \autoBeamOff
         bes4 bes r
%45
         \autoBeamOn g( bes) \autoBeamOff aes
         \autoBeamOn g( aes) \autoBeamOff c'
         \autoBeamOn bes2~(  bes8 aes) \autoBeamOff
         g2 g4
         g g g
%50
         g g r
         g g g
         f f r
         bes4.\mp bes8 bes4
         f'4. ees'8 d'4
%55
         bes4. bes8 bes bes
         bes2 bes4
         c'2.
         bes2 g4\p
         g g aes
%60
         aes8. aes16 g4 r
         c'4. c'8 c'4
         bes bes bes~ 
         bes2 bes4
         bes2.~ 
%65
         bes2 \autoBeamOn bes8( c') \autoBeamOff
         bes4 bes r
         \autoBeamOn g( bes) \autoBeamOff aes
         \autoBeamOn g( aes) \autoBeamOff c'
         bes2.~ 
%70
         bes
         bes
         \key bes \major \time 4/4  d'4.\f d'8 d'4 d'
         bes a a bes8 bes
         bes4 a a8. a16 bes8 bes
%75
         bes4 bes8 bes c'8. c'16 d'4
         c'2\p c'
         c' c'4\fermata c'\f
         bes bes bes8. bes16 bes8 bes
         \autoBeamOn c'( b c'4 d'8.) \autoBeamOff d'16\noBeam c'8 c'
%80
         bes4 bes8 a bes4 bes
         bes2 a
         bes4 \autoBeamOn bes8( a) \autoBeamOff g4 g
         c'2. bes4
         c'1
%85
         bes \bar "|."
}


BassMusic = {
          bes4.\f bes8 bes4 bes8 bes
         bes4.( bes,8) \autoBeamOff bes,2
         d4 d c bes,
         c2 f
%5
         bes,4.\p bes,8 ees4 c
         bes, bes, ees8. ees16 bes,4
         ees d c bes,
         f2 f4 f\f
         bes4. bes,8 bes,4 ees8 ees
%10
         d4 c8 c bes,2
         \autoBeamOn g4(\p f) \autoBeamOff \autoBeamOn e( f) \autoBeamOff
         c4. c8 f2
         f\f f4 f
         \autoBeamOn bes( aes g ees
%15
         f2) \autoBeamOff f
         bes,1
         bes,4.\p bes,8 bes,2
	 d4 bes, f4.( ees8)
         d2 c4 bes,
%20
         ees2 bes,
         bes4 a g f
         c' c8 c f4 f
         f\f f8. f16 f4 d
         g4. g8 g4 c
%25
         d d bes, bes,8 bes,
         c4. c8 f4 r
         d d8 d g4 g
         ees c d8. d16 d4
         g2\p c
%30
         d g
         d4\f d8 d g4 \autoBeamOn g8( f) \autoBeamOff
         ees4 c f f
         bes,8. bes,16 bes,4 ees \autoBeamOn ees8( d) \autoBeamOff
         \autoBeamOn c8.( d16 ees4 f8 ees) \autoBeamOff d\noBeam ees
%35
	  f2 bes,
	  \key ees \major
         \partial 256*64  \time 3/4  ees4\p
         ees ees aes,
         aes, ees ees
         aes2 a4
%40
         bes bes, r
         \autoBeamOn ees( f) \autoBeamOff g
         \autoBeamOn aes( g f
         ees f) \autoBeamOff \autoBeamOn g8( a) \autoBeamOff
         bes4 bes, r
%45
         ees2 ees4
         aes,2 aes,4
         bes,2.
         ees2 c4
         \autoBeamOn c2( d4) \autoBeamOff
%50
         c2 c4~ 
         c d ees
         f f r
         bes,4.\mp bes,8 bes,4
         f2 bes,4
%55
         ees2 ees4
          bes,2 bes,4
         f2.
         bes,2 ees4\p
         ees ees ees
%60
         ees8. ees16 ees4 ees
         aes8. aes16 \autoBeamOn aes4( a) \autoBeamOff
         bes bes, r
         \autoBeamOn ees( f) \autoBeamOff g
         \autoBeamOn aes( g f
%65
         ees f) \autoBeamOff \autoBeamOn g8( a) \autoBeamOff
         bes4 bes, r
         ees2 ees4
         aes,2 aes,4
         bes,2.~ 
%70
         bes,
         ees
         \key bes \major \time 4/4  bes4.\f bes8 bes4 bes
         bes f f bes,8 bes,
         bes4 f f8. f16 bes,8 bes,
%75
         ees4 bes,8 bes, f8. f16 bes,4
         ees2\p e
         f f4\fermata \autoBeamOn f8(\f ees) \autoBeamOff
         d4 bes, ees8. ees16 ees8 d
         \autoBeamOn c4.( ees8 f8.) \autoBeamOff f16\noBeam f8 ees
%80
         d4 c8 c bes,4 ees
         f2 f
         bes,4 bes, ees \autoBeamOn ees8( d) \autoBeamOff
         \autoBeamOn c4( ees f) \autoBeamOff bes,
         f1
%85
         bes, \bar "|."
}


TrebleLyrics = \lyricmode {
  Glo -- ri -- a in ex -- cel -- sis in ex -- cel -- sis De -- o,
  et in ter -- ra pax ho -- mi -- ni -- bus bo -- næ vo -- lun -- ta -- tis, Lau --
  da -- mus te, Be -- ne -- di -- ci -- mus te, A -- do -- ra -- mus te.  Glo -- ri -- fi --
  ca -- mus te.  Gra -- ti -- as a -- gi -- mus a -- gi -- mus
  ti -- bi prop -- ter mag -- nam glo -- ri -- am tu -- am Do -- mi ne De -- us rex cœ -- les -- tis,
  De -- us pa -- ter om -- ni -- po -- tens.  Do -- mi -- ne Fi -- li u -- ni -- ge -- ni -- te Je -- su Chris -- te.
  Do -- mi -- ne De -- us, ag -- nus De -- i, fi -- li -- us Pa -- tris fi -- li -- us Pa -- tris.
  Qui tol -- lis pec -- ca -- ta pec -- ca -- ta mun -- di mi -- se -- re -- re no -- bis, mi -- se -- re -- re no -- bis Qui tol -- lis pec --
  ca -- ta pec -- ca -- ta mun -- di Sus -- ci -- pe sus -- ci pe de -- pre -- ca -- ti -- o -- nem
  nos -- tram Qui se -- des ad dex -- ter -- am dex -- te -- ram Pa -- tris mi -- se
  re -- re  no -- bis mi -- se -- re -- re no -- bis.
  Quo -- ni -- am tu so -- lus Sanct -- us tu so -- lus Do -- mi -- nus tu so -- lus al -- tis -- si -- mus
  Je -- su Chris -- te, Cum sanc -- to spi -- ri -- tu in glo -- ri -- a in glo -- ri -- a De -- i
  Pa -- tris A -- men A -- men A -- men A -- men.
}
AltoLyrics = \lyricmode {
  Glo -- ri -- a in ex -- cel -- sis in ex -- cel -- sis De -- o,
  et in ter -- ra pax ho -- mi -- ni -- bus bo -- næ vo -- lun -- ta -- tis, Lau --
  da -- mus te, Be -- ne -- di -- ci -- mus te, A -- do -- ra -- mus te.  Glo -- ri -- fi --
  ca -- mus te.  Gra -- ti -- as a -- gi -- mus a -- gi -- mus
  ti -- bi prop -- ter mag -- nam glo -- ri -- am tu -- am Do -- mi ne De -- us rex cœ -- les -- tis,
  De -- us pa -- ter om -- ni -- po -- tens.  Do -- mi -- ne Fi -- li u -- ni -- ge -- ni -- te Je -- su Chris -- te.
  Do -- mi -- ne De -- us, ag -- nus De -- i, fi -- li -- us Pa -- tris fi -- li -- us Pa -- tris.
  Qui tol -- lis pec -- ca -- ta  mun -- di mi -- se -- re -- re no -- bis, mi -- se -- re -- re no -- bis Qui tol -- lis pec --
  ca -- ta pec -- ca -- ta mun -- di Sus -- ci -- pe sus -- ci pe de -- pre -- ca -- ti -- o -- nem
  nos -- tram Qui se -- des ad dex -- ter -- am dex -- te -- ram Pa -- tris mi -- se
  re -- re  no -- bis mi -- se -- re -- re no -- bis.
  Quo -- ni -- am tu so -- lus Sanct -- us tu so -- lus Do -- mi -- nus tu so -- lus al -- tis -- si -- mus
  Je -- su Chris -- te, Cum sanc -- to spi -- ri -- tu in glo -- ri -- a in glo -- ri -- a De -- i
  Pa -- tris A -- men A -- men A -- men A -- men.
}

TenorLyrics = \lyricmode {
  Glo -- ri -- a in ex -- cel -- sis in ex -- cel -- sis De -- o,
  et in ter -- ra pax ho -- mi -- ni -- bus bo -- næ vo -- lun -- ta -- tis, Lau --
  da -- mus te, Be -- ne -- di -- ci -- mus te, A -- do -- ra -- mus te.  Glo -- ri -- fi --
  ca -- mus te.  Gra -- ti -- as a -- gi -- mus a -- gi -- mus
  ti -- bi prop -- ter mag -- nam glo -- ri -- am tu -- am Do -- mi ne De -- us rex cœ -- les -- tis,
  De -- us pa -- ter om -- ni -- po -- tens.  Do -- mi -- ne Fi -- li u -- ni -- ge -- ni -- te Je -- su Chris -- te.
  Do -- mi -- ne De -- us, ag -- nus De -- i, fi -- li -- us Pa -- tris fi -- li -- us Pa -- tris.
  Qui tol -- lis pec -- ca -- ta  pec -- ca -- ta mun -- di mi -- se -- re -- re no -- bis, mi -- se -- re -- re no -- bis Qui tol -- lis pec --
  ca -- ta pec -- ca -- ta mun -- di Sus -- ci -- pe sus -- ci pe de -- pre -- ca -- ti -- o -- nem
  nos -- tram Qui se -- des ad dex -- ter -- am dex -- te -- ram Pa -- tris mi -- se
  re -- re  no -- bis mi -- se -- re -- re no -- bis.
  Quo -- ni -- am tu so -- lus Sanct -- us tu so -- lus Do -- mi -- nus tu so -- lus al -- tis -- si -- mus
  Je -- su Chris -- te, Cum sanc -- to spi -- ri -- tu in glo -- ri -- a in glo -- ri -- a De -- i
  Pa -- tris A -- men A -- men A -- men A -- men.
}
BassLyrics = \lyricmode {
Glo -- ri -- a in ex -- cel -- sis in ex -- cel -- sis De -- o,
  et in ter -- ra pax ho -- mi -- ni -- bus bo -- næ vo -- lun -- ta -- tis, Lau --
  da -- mus te, Be -- ne -- di -- ci -- mus te, A -- do -- ra -- mus te.  Glo -- ri -- fi --
  ca -- mus te.  Gra -- ti -- as a -- gi -- mus a -- gi -- mus
  ti -- bi prop -- ter mag -- nam glo -- ri -- am tu -- am Do -- mi ne De -- us rex cœ -- les -- tis,
  De -- us pa -- ter om -- ni -- po -- tens.  Do -- mi -- ne Fi -- li u -- ni -- ge -- ni -- te Je -- su Chris -- te.
  Do -- mi -- ne De -- us, ag -- nus De -- i, fi -- li -- us Pa -- tris fi -- li -- us Pa -- tris.
  Qui tol -- lis pec -- ca -- ta  pec -- ca -- ta mun -- di mi -- se -- re -- re no -- bis, mi -- se -- re -- re no -- bis Qui tol -- lis pec --
  ca -- ta  mun -- di Sus -- ci -- pe  de -- pre -- ca -- ti -- o -- nem
  nos -- tram Qui se -- des ad dex -- ter -- am ad dex -- te -- ram Pa -- tris mi -- se
  re -- re  no -- bis mi -- se -- re -- re no -- bis.
  Quo -- ni -- am tu so -- lus Sanct -- us tu so -- lus Do -- mi -- nus tu so -- lus al -- tis -- si -- mus
  Je -- su Chris -- te, Cum sanc -- to spi -- ri -- tu in glo -- ri -- a in glo -- ri -- a De -- i
  Pa -- tris A -- men A -- men A -- men A -- men.
}

initialKeyTime = {\autoBeamOff \key bes \major \time 4/4}
\include "template.ly"