origLineBreak={| \break}
origPageBreak={\pageBreak}
origNoLineBreak={\noBreak}
% lineBreakSetting =
% #(define-music-function
%   (parser location l)
%   (ly:music?)			%is it?
%   #{
%   \autoLineBreaksOff
%   #}
% )
\paper{
  top-margin = 5\cm
  bottom-margin = 3\cm
  ragged-last-bottom = ##t
}
\header{
  title = "Proofreading Copy"
  subtitle = "Line and page breaks identical with original"
}

\include "kyrie.ly"
\include "gloria.ly"
\include "credo.ly"
\include "sanctus.ly"
\include "agnus.ly"